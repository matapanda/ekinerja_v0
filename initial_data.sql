-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: ekinerja
-- ------------------------------------------------------
-- Server version	5.7.11-0ubuntu6

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `jabatan`
--


--
-- Dumping data for table `instansi`
--


--
-- Dumping data for table `satuan_jumlah`
--

LOCK TABLES `satuan_jumlah` WRITE;
/*!40000 ALTER TABLE `satuan_jumlah` DISABLE KEYS */;
INSERT INTO `satuan_jumlah` VALUES (1,'lembar'),(2,'dokumen'),(3,'pertemuan');
/*!40000 ALTER TABLE `satuan_jumlah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `satuan_waktu`
--

LOCK TABLES `satuan_waktu` WRITE;
/*!40000 ALTER TABLE `satuan_waktu` DISABLE KEYS */;
INSERT INTO `satuan_waktu` VALUES (1,'hari'),(2,'bulan'),(3,'tahun');
/*!40000 ALTER TABLE `satuan_waktu` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `gaji_golongans` WRITE;
/*!40000 ALTER TABLE `gaji_golongans` DISABLE KEYS */;
INSERT INTO `gaji_golongans` VALUES (1,1,0),(2,2,0),(3,3,0),(4,4,0),(5,5,0),(6,6,0),(7,7,0),(8,8,0),(9,9,0),(10,10,0),(11,11,0),(12,12,0),(13,13,0),(14,14,0),(15,15,0),(16,16,0),(17,17,0);
/*!40000 ALTER TABLE `gaji_golongans` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Dumping data for table `pegawai`
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-19  9:40:08
