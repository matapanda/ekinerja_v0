<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SatuanWaktu extends Model
{
    //
    protected $table = 'satuan_waktu';

    public $timestamps = false;

    protected $fillable   = ['nama'];
}
