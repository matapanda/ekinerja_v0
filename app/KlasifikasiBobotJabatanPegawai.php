<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KlasifikasiBobotJabatanPegawai extends Model
{
    //
    public $timestamps = false;

        protected $fillable = ['id_pegawai', 'id_bobot_jabatan'];

}
