<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiSKPBulanan extends Model
{
    //
    protected $table = 'nilai_skp_bulanan';

    public $timestamps = false;

    protected $fillable = ['pegawai_id', 'bulan', 'tahun'];
}
