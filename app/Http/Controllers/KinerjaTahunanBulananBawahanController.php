<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Request as Req;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\SatuanJumlah;
use App\SatuanWaktu;
use App\KinerjaTahunan;
use App\KinerjaBulanan;
use App\Pegawai;
use App\Util;
use App\Posisi;
use App\Setting;

use PHPExcel; 
use PHPExcel_IOFactory;
use Log;

use Collective\Html\FormFacade as Form;

class KinerjaTahunanBulananBawahanController extends ModelController
{
    protected $pegawai = null;

    protected $page_title = 'Rancangan Kegiatan Tahunan Bawahan';

    protected $index_view = 'index_tahunan_bulanan_bawahan';

    protected $form_view = 'form_kinerja';

    protected $form_view_bulanan = 'form_kinerja_bulanan';

    protected $model = '\\App\\KinerjaTahunan';

    public function getIndex()
    {
        $list_pegawai = array(0 => 'Pilih Pegawai');
        foreach($this->pegawai->getBawahanLangsung()->get() as $bwh){
          $list_pegawai[$bwh->id_pegawai] = $bwh->nip_baru.' - '.$bwh->nama;
        }

        $list_tahun = array();       

        $index_view = (isset($this->index_view) ? 
            $this->index_view : 'index');

        $extra_context = $this->get_extra_context();

       return view($index_view)
                ->with('page_title',$this->page_title)
                ->with('list_pegawai', $list_pegawai)
                ->with('list_tahun', $list_tahun)
                ->with('extra_context', $extra_context)
                ->with('base_url',action(class_basename($this)."@getIndex"))
                ->with($extra_context);
    }

    protected function define_fields($edit){  
      $pegawai = $this->pegawai;
      $user_id = $pegawai->getPegawaiId();
      $tahun = Carbon::today()->format('Y');
      $edit->add('nama','Nama Kegiatan', 'text')->rule('required');
      $edit->add('deskripsi','Deskripsi Kegiatan', 'textarea');
      
      if($edit->status == 'create'){
        $edit->set('pegawai_id', $user_id);
        $edit->set('tahun', $tahun);
        $edit->set('posisi', $pegawai->getPosisiIdTerakhir());
      }
      else{
        $edit->add('tahun','Tahun', 'text')->mode('readonly');
      }

      $edit->add('target_kuantitas','Target Kuantitas', 'text')->rule('required');
      $edit->add('satuan_target_kuantitas','<br/>','select')->options(SatuanJumlah::lists('nama', 'id')->all());
      $edit->add('target_waktu','Target Waktu', 'text')->rule('required');
      $edit->set('satuan_target_waktu', 2);
      $edit->add('target_kualitas','Target Kualitas (%)', 'text')->rule('required');

      if($edit->status == 'show'){
        $edit->text('capaian_kuantitas','Capaian Kuantitas')->mode('readonly');
        $edit->text('capaian_waktu','Capaian Waktu')->mode('readonly');
        $edit->text('capaian_kualitas','Capaian Kualitas (%)')->mode('readonly');
      }
     

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to($this->getRedirectUrl())->with('message', 'Success');
      });         
    }

    public function getTahunanbulanan(Req $request){
        $pegawai_id =  $request->get('pegawai');
        
        $tahun = $request->get('tahun');        
       
        $posisi = $request->get('posisi');
        
        $kinerja_tahunan = KinerjaTahunan::select('*', 'satuan_jumlah.nama as nama_jumlah', 'satuan_waktu.nama as nama_waktu', 'kinerja_tahunan.nama as nama_kegiatan', 'kinerja_tahunan.id as kinerja_id')
                ->where('pegawai_id', $pegawai_id)
                ->leftJoin('satuan_jumlah', 'satuan_target_kuantitas', '=', 'satuan_jumlah.id')
                ->leftJoin('satuan_waktu', 'satuan_target_waktu', '=', 'satuan_waktu.id');

        
        if($tahun){
          $kinerja_tahunan = $kinerja_tahunan->where('tahun', $tahun);
        }
        if($posisi){
          $kinerja_tahunan = $kinerja_tahunan->where('posisi', $posisi);
        }
        $kinerja_tahunan = $kinerja_tahunan->get();

        $arr_kinerja = array();
        foreach($kinerja_tahunan as $kt){
          $presentase_capaian_kuantitas_tahunan = ($kt->capaian_kuantitas/$kt->target_kuantitas)*100;
          $status = 'BT';
          if($presentase_capaian_kuantitas_tahunan >= 100){
            $status = 'T';
          }
          $arr_kinerja[] = array(
              "id" => $kt->kinerja_id,
              "nama" => '<b><a href="#" onclick=showDetail("/kinerja_tahunan_bulanan_bawahan/form",'.$kt->kinerja_id.')>'.$kt->nama_kegiatan.'</a></b>',
              "tahun_bulan" => "<p></p>",
              "target_kuantitas" => $kt->target_kuantitas." ".$kt->nama_jumlah,
              "target_waktu" => $kt->target_waktu." ".$kt->nama_waktu,
              //"target_kualitas" => $kt->target_kualitas." %",
              "realisasi_kuantitas" => $kt->capaian_kuantitas.' ('.$presentase_capaian_kuantitas_tahunan.'%)',
              "realisasi_kualitas" => $kt->capaian_kualitas.'%',
              "status" => $status              
            );

          $kinerja_bulanan = KinerjaBulanan::select('*', 'satuan_jumlah.nama as nama_jumlah', 'satuan_waktu.nama as nama_waktu', 'kinerja_bulanan.nama as nama_kegiatan', 'kinerja_bulanan.id as kinerja_id')
                ->orderby('bulan', 'asc')
                ->leftJoin('satuan_jumlah', 'satuan_target_kuantitas', '=', 'satuan_jumlah.id')
                ->leftJoin('satuan_waktu', 'satuan_target_waktu', '=', 'satuan_waktu.id')
                ->where('kinerja_tahunan_id', $kt->kinerja_id)->get();
          
          foreach($kinerja_bulanan as $kb){
              $presentase_capaiain = ($kb->capaian_kuantitas/$kb->target_kuantitas)*100;
              $status = 'BT';
              if($presentase_capaiain >= 100){
                $status = 'T';
              }
              $arr_kinerja[] = array(
              "id" => $kb->kinerja_id,
              "nama" => '<p style="padding-left : 15px"><a href="#" onclick=showDetail("/kinerja_tahunan_bulanan_bawahan/formbulanan",'.$kb->kinerja_id.')>'.$kb->nama_kegiatan.'</a></p>',
              "tahun_bulan" => Util::get_bulan($kb->bulan),
              "target_kuantitas" => $kb->target_kuantitas." ".$kb->nama_jumlah,
              "target_waktu" => $kb->target_waktu." ".$kb->nama_waktu,
              //"target_kualitas" => $kb->target_kualitas." %",
              "realisasi_kuantitas" => $kb->capaian_kuantitas.' ('.$presentase_capaiain.'%)',
              "realisasi_kualitas" => $kb->capaian_kualitas.'%',
              "status" => $status,
              
              );            
          }
        }

        $grid = \DataGrid::source($arr_kinerja);
        $grid->paginate($this->pagination);
        $grid->add('nama','Nama Kegiatan');
        $grid->add('tahun_bulan','Bulan');
        $grid->add('target_waktu','Target Waktu');
        $grid->add('target_kuantitas','Target Kuantitas');   
        $grid->add('realisasi_kuantitas','Realisasi Kuantitas');
        $grid->add('realisasi_kualitas','Realisasi Kualitas');     
        $grid->add('status','Status');

        return View::make('ajax.tabel_tahunan_bulanan')
                          ->with('grid' , $grid);  
  }

  public function anyFormbulanan($id_tahunan=null){
        $edit = \DataEdit::source(new KinerjaBulanan);

        if(!$this->checkFormPermissionBulanan($edit)){
            if(Request::ajax()){
                return response('Halaman tidak berhak diakses', 401);
            }
            else{
                return Redirect::to('/')->with('message', 'Halaman tidak berhak diakses');
            }
        }

        $page_activity = 'Tambah';
        if($edit->status == 'modify'){
            $page_activity = 'Ubah';
        }
        else if($edit->status == 'show'){
            $page_activity = 'Detail';
        }

        $this->define_fields_bulanan($edit, $id_tahunan);
        $extra_context = $this->get_extra_context();
        $form_view = $this->form_view_bulanan;
        
        if(Request::ajax()){            
            $edit->build();
            // If untuk menampilkan form
            if($edit->action == 'idle'){            
              $content_type = Request::header('Accept');
              switch ($edit->status) {
                  case 'create':
                  case 'modify' :
                  case 'delete':
                  case 'show':
                      if($content_type == 'application/json'){
                          $data_array = array();
                          foreach ($edit->fields as $field) {
                              $data_array[$field->name] = $field->value;
                          }
                          return response()->json($data_array);
                      }
                      else{
                          $form_view = 'ajax.'.$this->form_view_bulanan;
                      }
                      break;
                  default:
                      return response()->json(array("err" => "unknown"), 404);
              }           
            }
            // Else, kerjakan action dan cek statusnya
            else{
              if($edit->process_status == 'success'){
                return response()->json(array("status"=>$edit->process_status));
              }
              else{
                return response()->json(array("status"=>$edit->process_status),400);
              }
            }
        }
        
        $result = $edit->view($form_view)->with(compact('edit'))
              ->with('page_title','Rancangan Kegiatan Bulanan')
              ->with('page_activity',$page_activity)
              ->with('base_url',action(class_basename($this)."@getIndex"))
              ->with($extra_context);     
        return $result;
    }

    protected function define_fields_bulanan($edit, $id_tahunan=null){
      $tahun = Carbon::today()->format('Y');
      $user_id = $this->pegawai->getPegawaiId();
      if($edit->status != 'create'){
        $edit->add('kinerja_tahunan.tahun','Tahun', 'autocomplete')->mode('readonly');
      }
      if($edit->status == 'create'){
        $edit->set('pegawai_id', $user_id);
        $edit->set('tahun', $tahun);
      }
      $edit->add('nama','Nama Kegiatan', 'text')->rule('required');
      $edit->add('deskripsi','Deskripsi Kegiatan', 'redactor');
      $edit->add('bulan','Bulan','select')->options(Util::get_all_bulan());
      $edit->add('target_kuantitas','Target Kuantitas', 'text')->rule('required');
      $edit->add('satuan_target_kuantitas','<br/>','select')->options(SatuanJumlah::lists('nama', 'id')->all());
      $edit->add('target_waktu','Target Waktu', 'text')->rule('required');
      $edit->add('satuan_target_waktu','<br/>','select')->options(SatuanWaktu::whereIn('nama', array('hari', 'jam'))->lists('nama', 'id')->all());
      $edit->add('target_kualitas','Target Kualitas (%)', 'text')->rule('required');
      if($edit->status == 'show'){
        $edit->add('kinerja_tahunan','Kegiatan Tahunan','select')->options(KinerjaTahunan::where('tahun', $tahun)->where('pegawai_id', $user_id)->lists('nama', 'id'));
      }
      else{
        if($id_tahunan == null){
          $edit->add('kinerja_tahunan','Kegiatan Tahunan','select')->options(KinerjaTahunan::where('tahun', $tahun)->where('pegawai_id', $user_id)->lists('nama', 'id'));
        }
        else{
          
          $edit->add('kinerja_tahunan','Kegiatan Tahunan','select')->options(KinerjaTahunan::where('id', $id_tahunan)->where('tahun', $tahun)->where('pegawai_id', $user_id)->lists('nama', 'id'));
        }  
      }
      
      
      if($edit->status == 'show'){
        $edit->text('capaian_kuantitas','Capaian Kuantitas')->mode('readonly');
        $edit->text('capaian_waktu','Capaian Waktu')->mode('readonly');
        $edit->text('capaian_kualitas','Capaian Kualitas (%)')->mode('readonly');
      }

      $edit->link($this->getRedirectUrl(),trans('rapyd::rapyd.back'))->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to($this->getRedirectUrl())->with('message', 'Success');
      }); 
    }

  public function getTahun(Req $request){
    $pegawai_id = $request->get('pegawai');
    $list_tahun = KinerjaTahunan::select('tahun')->where('pegawai_id', $pegawai_id)->distinct('tahun')->orderby('tahun', 'desc')->lists('tahun', 'tahun');
    return response()->json($list_tahun);
  }

  protected function get_extra_context(){
        $extra_context = parent::get_extra_context();
        $tahun = Carbon::today()->format('Y');
        $extra_context['tahun'] = $tahun;
        return $extra_context;
  }

  protected function checkFormPermission($edit){
    if($edit->status == 'show'){
      $kinerja = KinerjaTahunan::where('id', $edit->model->id)->first();
      if(!$kinerja){
        return false;
      }
      else{
        if(!$this->pegawai->isAtasan($kinerja->pegawai_id)){
          return false;
        }
      }
    }
    else{
        return false;
    }    
    return true;
  }

  protected function checkFormPermissionBulanan($edit){
    if($edit->status == 'show'){
      $kinerja = KinerjaBulanan::where('id', $edit->model->id)->first();
      if(!$kinerja){
        return false;
      }
      else{
        if(!$this->pegawai->isAtasan($kinerja->pegawai_id)){
          return false;
        }
      }
    }
    else{
        return false;
    }    
    return true;
  }

  protected function getRedirectUrl(){
    return 'kinerja_tahunan_bulanan';
  }

  public function getExportskp($tahun=null)
  {
    $id_pegawai = $this->pegawai->getPegawaiId();
    if($tahun==null){
      $tahun=date("Y");
    }

    $tahun=date("Y");

    $kinerja_tahunan = KinerjaTahunan::select('*', 'kinerja_tahunan.nama', 'satuan_waktu.nama as nama_satuan_waktu', 'satuan_jumlah.nama as nama_satuan_jumlah')
        ->leftJoin('satuan_waktu', 'kinerja_tahunan.satuan_target_waktu', '=', 'satuan_waktu.id')
        ->leftJoin('satuan_jumlah', 'kinerja_tahunan.satuan_target_kuantitas', '=', 'satuan_jumlah.id')
        ->where('kinerja_tahunan.pegawai_id', $id_pegawai)
        ->where('kinerja_tahunan.tahun', $tahun)
        ->orderby('kinerja_tahunan.id', 'asc')
        ->get();

      $nama_pegawai = $this->pegawai->gelar_depan." ".$this->pegawai->nama." ".$this->pegawai->gelar_belakang;
      $nip_pegawai = $this->pegawai->nip_baru;
      $instansi_pegawai = $this->pegawai->getUnitK()->keterangan;
      $jabatan_pegawai = $this->pegawai->getPosisiTerakhir()->keterangan;
      $pangkat_pegawai = $this->pegawai->getGolRuangTerakhir()->keterangan;

      $atasan = $this->pegawai->getAtasanLangsung();
      if($atasan == null || sizeof($atasan)==0){
        $nama_atasan = '-';
        $nip_atasan = '-';
        $jabatan_atasan = '-';
        $instansi_atasan = '';
        $pangkat_atasan = '-'; 
      }
      else{
        $nama_atasan = $atasan->gelar_depan." ".$atasan->nama." ".$atasan->gelar_belakang;
        $nip_atasan = $atasan->nip_baru;        
        $instansi_atasan = $atasan->keterangan_unitk;
        $jabatan_atasan = $atasan->keterangan_posisi;
        $pangkat_atasan = $atasan->getGolRuangTerakhir()->keterangan;  
      }

      $objPHPExcel = PHPExcel_IOFactory::load(base_path().'/public/TemplateFormSKP2.xls');
      
      $sheet1 = $objPHPExcel->setActiveSheetIndex(0);
      $sheet2 = $objPHPExcel->setActiveSheetIndex(1);
      $sheet3 = $objPHPExcel->setActiveSheetIndex(2);

      $sheet1->setCellValue('D5', $nama_atasan);
      $sheet1->setCellValue('D6', $nip_atasan);
      $sheet1->setCellValue('D7', $pangkat_atasan);
      $sheet1->setCellValue('D8', $jabatan_atasan);
      $sheet1->setCellValue('D9', $instansi_atasan);
      
      $sheet1->setCellValue('I5', $nama_pegawai);
      $sheet1->setCellValue('I6', $nip_pegawai);
      $sheet1->setCellValue('I7', $pangkat_pegawai);
      $sheet1->setCellValue('I8', $jabatan_pegawai);
      $sheet1->setCellValue('I9', $instansi_pegawai);

      $sheet1->setCellValue('H16', 'Malang, 02 Januari '.$tahun);
      $sheet2->setCellValue('M20', 'Malang, 31 Desember '.$tahun);

      $sheet3->setCellValue('E39', '9. DIBUAT TANGGAL, 02 JANUARI '.$tahun); 
      $sheet3->setCellValue('C44', 'DITERIMA TANGGAL, 05 JANUARI '.$tahun); 
      $sheet3->setCellValue('E49', '11.DITERIMA TANGGAL, 05 JANUARI '.$tahun); 
      $sheet3->setCellValue('R36', ': 02 Januari s/d 31 Desember '.$tahun);

      

      $sheet3->setCellValue('P50', $nama_atasan); 
      $sheet3->setCellValue('P51', $nip_atasan); 
      $sheet3->setCellValue('P52', $pangkat_atasan); 
      $sheet3->setCellValue('P53', $jabatan_atasan); 
      $sheet3->setCellValue('P54', $instansi_atasan); 
      
      $row=13;
      $row2=9;
      $nomor=1;

      foreach ($kinerja_tahunan as $item) {
        if ($nomor==1){
          $sheet1->setCellValue('B12', $nomor);
          $sheet1->setCellValue('C12', $item->nama);
          $sheet1->setCellValue('F12', '=E12*G12');
          $sheet1->setCellValue('G12', $item->target_kuantitas);
          $sheet1->setCellValue('H12', $item->nama_satuan_jumlah);
          $sheet1->setCellValue('I12', $item->target_kualitas);
          $sheet1->setCellValue('J12', $item->target_waktu);
          $sheet1->setCellValue('K12', $item->nama_satuan_waktu);
          $sheet1->setCellValue('L12', '0');

          $sheet2->setCellValue('K8', $item->capaian_kuantitas);
          $sheet2->setCellValue('L8', '=E8');
          $sheet2->setCellValue('M8', $item->capaian_kualitas);
          $sheet2->setCellValue('N8', $item->capaian_waktu);
          $sheet2->setCellValue('O8', '=H8');
          $sheet2->setCellValue('P8', '0');

          $sheet2->setCellValue('T8', '=IF(D8>0,1,0)');
          $sheet2->setCellValue('U8', '=R8');
          $sheet2->setCellValue('W8', '=100-(N8/G8*100)');
          $sheet2->setCellValue('X8', '=IF(I8=0,0,100-(P8/I8*100))');
          $sheet2->setCellValue('Y8', '=K8/D8*100');
          $sheet2->setCellValue('Z8', '=M8/F8*100');
          $sheet2->setCellValue('AA8', '=IF(W8>24,AD8,AC8)');
          $sheet2->setCellValue('AB8', '=IF(X8>24,AF8,AE8)');
          $sheet2->setCellValue('AC8', '=((1.76*G8-N8)/G8)*100');
          $sheet2->setCellValue('AD8', '=76-((((1.76*G8-N8)/G8)*100)-100)');
          $sheet2->setCellValue('AE8', '=IF(I8=0,0,((1.76*I8-P8)/I8)*100)');
          $sheet2->setCellValue('AF8', '=IF(I8=0,0,76-((((1.76*I8-P8)/I8)*100)-100))');
          $sheet2->setCellValue('AG8', '=SUM(Y8:AB8)');

          $sheet2->setCellValue('Q8', '=AG8');
          $sheet2->setCellValue('R8', '=IF(I8="-",IF(P8="-",Q8/3,Q8/4),Q8/4)');

          
        }
        else
        {
          $sheet1->insertNewRowBefore($row, 1);
          $sheet1->mergeCells('C'.$row.':D'.$row.'');
          $sheet1->setCellValue('B'.$row, $nomor);
          $sheet1->setCellValue('C'.$row, $item->nama);   
          $sheet1->setCellValue('F'.$row, '=E'.$row.'*G'.$row.'');   
          
          $sheet1->setCellValue('G'.$row, $item->target_kuantitas);
          $sheet1->setCellValue('H'.$row, $item->nama_satuan_jumlah);
          $sheet1->setCellValue('I'.$row, $item->target_kualitas);
          $sheet1->setCellValue('J'.$row, $item->target_waktu);
          $sheet1->setCellValue('K'.$row, $item->nama_satuan_waktu);
          $sheet1->setCellValue('L'.$row, '0');

          $sheet2->insertNewRowBefore($row2, 1);

          $sheet2->setCellValue('A'.$row2, $nomor);
          $sheet2->setCellValue('B'.$row2, '=SKP!C'.$row);
          $sheet2->setCellValue('C'.$row2, '=SKP!F'.$row);
          $sheet2->setCellValue('D'.$row2, '=SKP!G'.$row);
          $sheet2->setCellValue('E'.$row2, '=SKP!H'.$row);
          $sheet2->setCellValue('F'.$row2, '=SKP!I'.$row);
          $sheet2->setCellValue('G'.$row2, '=SKP!J'.$row);
          $sheet2->setCellValue('H'.$row2, '=SKP!K'.$row);
          $sheet2->setCellValue('I'.$row2, '=SKP!L'.$row);
          $sheet2->setCellValue('J'.$row2, '=K'.$row2.'*SKP!E'.$row);
          $sheet2->setCellValue('K'.$row2, $item->capaian_kuantitas);
          $sheet2->setCellValue('L'.$row2, '=E'.$row2);
          $sheet2->setCellValue('M'.$row2, $item->capaian_kualitas);
          $sheet2->setCellValue('N'.$row2, $item->capaian_waktu);
          $sheet2->setCellValue('O'.$row2, '=H'.$row2);
          $sheet2->setCellValue('P'.$row2, '0');

          $sheet2->setCellValue('T'.$row2.'', '=IF(D'.$row2.'>0,1,0)');
          $sheet2->setCellValue('U'.$row2.'', '=R'.$row2.'');
          $sheet2->setCellValue('W'.$row2.'', '=100-(N'.$row2.'/G'.$row2.'*100)');
          $sheet2->setCellValue('X'.$row2.'', '=IF(I'.$row2.'=0,0,100-(P'.$row2.'/I'.$row2.'*100))');
          $sheet2->setCellValue('Y'.$row2.'', '=K'.$row2.'/D'.$row2.'*100');
          $sheet2->setCellValue('Z'.$row2.'', '=M'.$row2.'/F'.$row2.'*100');
          $sheet2->setCellValue('AA'.$row2.'', '=IF(W'.$row2.'>24,AD'.$row2.',AC'.$row2.')');
          $sheet2->setCellValue('AB'.$row2.'', '=IF(X'.$row2.'>24,AF'.$row2.',AE'.$row2.')');
          $sheet2->setCellValue('AC'.$row2.'', '=((1.76*G'.$row2.'-N'.$row2.')/G'.$row2.')*100');
          $sheet2->setCellValue('AD'.$row2.'', '=76-((((1.76*G'.$row2.'-N'.$row2.')/G'.$row2.')*100)-100)');
          $sheet2->setCellValue('AE'.$row2.'', '=IF(I'.$row2.'=0,0,((1.76*I'.$row2.'-P'.$row2.')/I'.$row2.')*100)');
          $sheet2->setCellValue('AF'.$row2.'', '=IF(I'.$row2.'=0,0,76-((((1.76*I'.$row2.'-P'.$row2.')/I'.$row2.')*100)-100))');
          $sheet2->setCellValue('AG'.$row2.'', '=SUM(Y'.$row2.':AB'.$row2.')');

          $sheet2->setCellValue('Q'.$row2.'', '=AG'.$row2.'');
          $sheet2->setCellValue('R'.$row2.'', '=IF(I'.$row2.'="-",IF(P'.$row2.'="-",Q'.$row2.'/3,Q'.$row2.'/4),Q'.$row2.'/4)');

          
          $row++;
          $row2++;
        }
        $nomor++;
          
        }
        $sumrow=$nomor+16;
        $sumrow1=$sumrow-1;
        

        //$sheet2->setCellValue('R'.$sumrow.'', '=IF(R'.$sumrow1.'<=50,"(Buruk)",IF(R'.$sumrow1.'<=60,"(Sedang)",IF(R'.$sumrow1.'<=75,"(Cukup)",IF(R'.$sumrow1.'<=90.99,"(Baik)","(Sangat Baik)"))))');
        $sheet2->setCellValue('R'.$sumrow.'', '=IF(R'.$sumrow1.'<=50,"Buruk",IF(R'.$sumrow1.'<=60,"Sedang",IF(R'.$sumrow1.'<=75,"Cukup",IF(R'.$sumrow1.'<=90.99,"Baik","Sangat Baik"))))');
                    
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    //$objWriter->save('write.xls');
    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=\"SKP ".$this->pegawai->nama." ".$tahun." .xls\"");
    header("Cache-Control: max-age=0");
    $objWriter->save("php://output");
  }

  public function postImportskp()
  {
    $pegawai_id = $this->pegawai->getPegawaiId();
    $tahun = Carbon::today()->format('Y');

    if(Input::hasFile('skp')){
      $objPHPExcel = PHPExcel_IOFactory::load(Input::file('skp'));
      
      $sheet1 = $objPHPExcel->setActiveSheetIndex(0);
      $sheet2 = $objPHPExcel->setActiveSheetIndex(1);
      $sheet3 = $objPHPExcel->setActiveSheetIndex(2);

      $temp_tahun = $sheet2->getCellByColumnAndRow(0,4)->getValue();
      //$tahun = substr($temp_tahun, strrpos($temp_tahun, ' ') + 1);
      //echo $tahun;
      //echo '<br>';
      $kinerja_tahunan = null;
      $num_row=1;
      $kinerja='-';      
      while ($kinerja!='')
      {
          echo $num_row.' '; 

          $num_row_target=$num_row+11;


          $kinerja = $sheet1->getCellByColumnAndRow(2,$num_row_target)->getValue();
          if ($kinerja!='')
          {
            echo $kinerja."\t";
          
            $ak = $sheet1->getCellByColumnAndRow(5,$num_row_target)->getCalculatedValue();
            if ($ak=='')$ak=0;
            echo $ak."\t";
            $kuantitas = $sheet1->getCellByColumnAndRow(6,$num_row_target)->getValue();
            if ($kuantitas=='')$kuantitas=0;
            echo $kuantitas."\t";
            $satuan_kuantitas = $sheet1->getCellByColumnAndRow(7,$num_row_target)->getValue();
            echo $satuan_kuantitas."\t";
            $kualitas = $sheet1->getCellByColumnAndRow(8,$num_row_target)->getValue();
            if ($kualitas=='')$kualitas=0;
            echo $kualitas."\t";
            $waktu = $sheet1->getCellByColumnAndRow(9,$num_row_target)->getValue();
            if ($waktu=='')$waktu=0;
            echo $waktu."\t";
            $satuan_waktu = $sheet1->getCellByColumnAndRow(10,$num_row_target)->getValue();
            echo $satuan_waktu."\t";
            $biaya = $sheet1->getCellByColumnAndRow(11,$num_row_target)->getValue();
            if ($biaya=='' || $biaya=='-')$biaya=0;
            echo $biaya."\t";

            


            $id_satuan_kuantitas = SatuanJumlah::firstOrCreate(['nama' => $satuan_kuantitas]);
            $id_satuan_waktu = SatuanWaktu::firstOrCreate(['nama' => $satuan_waktu]);

            echo $id_satuan_kuantitas->nama;

            echo "<br>";

            $bulan = $sheet1->getCellByColumnAndRow(12,$num_row_target)->getValue();

            
            if($bulan == ''){

              $num_row_real=$num_row+7;

              /*
              $real_ak = $sheet2->getCellByColumnAndRow(9,$num_row_real)->getCalculatedValue();
              if ($real_ak=='')$real_ak=0;
              echo $real_ak."\t";
              $real_kuantitas = $sheet2->getCellByColumnAndRow(10,$num_row_real)->getValue();
              if ($real_kuantitas=='')$real_kuantitas=0;
              echo $real_kuantitas."\t";
              $real_kualitas = $sheet2->getCellByColumnAndRow(12,$num_row_real)->getValue();
              if ($real_kualitas=='')$real_kualitas=0;
              echo $real_kualitas."\t";
              $real_waktu = $sheet2->getCellByColumnAndRow(13,$num_row_real)->getValue();
              if ($real_waktu=='')$real_waktu=0;
              echo $real_waktu."\t";
              $real_biaya = $sheet2->getCellByColumnAndRow(15,$num_row_real)->getValue();
              if ($real_biaya=='' || $real_biaya=='-')$real_biaya=0;
              echo $real_biaya."\t";

              $perhitungan = $sheet2->getCellByColumnAndRow(16,$num_row_real)->getOldCalculatedValue();
              if ($perhitungan=='' || $perhitungan=='-')$perhitungan=0;
              echo $perhitungan."\t";

              $nilai_capaian = $sheet2->getCellByColumnAndRow(17,$num_row_real)->getOldCalculatedValue();
              if ($nilai_capaian=='' || $nilai_capaian=='-')$nilai_capaian=0;
              echo $nilai_capaian."\t";
              */
  
              $kinerja_tahunan = KinerjaTahunan::firstOrNew(['nama' => $kinerja, 'pegawai_id' => $pegawai_id, 'tahun' => $tahun]);
              
              $kinerja_tahunan->nama=$kinerja;
              $kinerja_tahunan->pegawai_id=$pegawai_id;
              $kinerja_tahunan->tahun = $tahun;
              $kinerja_tahunan->target_kuantitas = $kuantitas;
              $kinerja_tahunan->satuan_target_kuantitas = $id_satuan_kuantitas->id;
              $kinerja_tahunan->target_waktu = $waktu;
              $kinerja_tahunan->satuan_target_waktu = $id_satuan_waktu->id;
              $kinerja_tahunan->target_kualitas = $kualitas;
              $kinerja_tahunan->status_target = 1;
              $kinerja_tahunan->posisi = $this->pegawai->getPosisiIdTerakhir();

              //$kinerja_tahunan->capaian_kuantitas = $real_kuantitas;
              //$kinerja_tahunan->capaian_waktu = $real_waktu;
              //$kinerja_tahunan->capaian_kualitas = $real_kualitas;
              //$kinerja_tahunan->status_capaian = 1;
              //$kinerja_tahunan->skor = $nilai_capaian;
              //$kinerja_tahunan->status=1;

              $kinerja_tahunan->save();
            }
            else{
              if($kinerja_tahunan){
                $id_bulan = Util::get_bulan_inverse($bulan);
                $kinerja_bulanan = KinerjaBulanan::firstOrNew(['nama' => $kinerja, 'pegawai_id' => $pegawai_id, 'kinerja_tahunan_id' => $kinerja_tahunan->id]);
                
                $kinerja_bulanan->nama=$kinerja;
                $kinerja_bulanan->pegawai_id=$pegawai_id;
                $kinerja_bulanan->target_kuantitas = $kuantitas;
                $kinerja_bulanan->satuan_target_kuantitas = $id_satuan_kuantitas->id;
                $kinerja_bulanan->target_waktu = $waktu;
                $kinerja_bulanan->satuan_target_waktu = $id_satuan_waktu->id;
                $kinerja_bulanan->target_kualitas = $kualitas;
                $kinerja_bulanan->status_target = 1;
                $kinerja_bulanan->kinerja_tahunan_id = $kinerja_tahunan->id;
                $kinerja_bulanan->bulan = $id_bulan;
                //$kinerja_tahunan->capaian_kuantitas = $real_kuantitas;
                //$kinerja_tahunan->capaian_waktu = $real_waktu;
                //$kinerja_tahunan->capaian_kualitas = $real_kualitas;
                //$kinerja_tahunan->status_capaian = 1;
                //$kinerja_tahunan->skor = $nilai_capaian;
                //$kinerja_tahunan->status=1;

                $kinerja_bulanan->save();
              }
            }
          }          
          $num_row++;
      }
      return Redirect::to($this->getRedirectUrl())->with('message', 'Upload file berhasil');
    }
    else{
      return Redirect::to($this->getRedirectUrl())->with('message', 'Upload file gagal');
    }    
  }



  public function getProfilpegawai(Req $request){
    $pegawai_id = $request->get('pegawai');
    $pegawai = Util::getPegawaiWithPosisi($pegawai_id)->first();
    Log::info("Hai ".$pegawai->nama);
    if(!$pegawai){
      return FacadeResponse::make('Pegawai tidak ditemukan', 404);
    }
    if(!$this->pegawai->isAtasan($pegawai_id)){
      return FacadeResponse::make('Halaman tidak berhak diakses', 401);
    }
    return View::make('ajax.profil_pegawai')
                        ->with('pegawai' , $pegawai);
  }
}
