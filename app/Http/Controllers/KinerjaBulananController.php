<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\SatuanJumlah;
use App\SatuanWaktu;
use App\KinerjaTahunan;
use App\Util;
use App\KinerjaBulanan;
use App\NilaiSKPBulanan;
use App\Setting;
use Illuminate\Support\Facades\View;

class KinerjaBulananController extends KinerjaController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $model = '\\App\\KinerjaBulanan';

    protected $form_view = 'form_kinerja_bulanan';

    protected $page_title = 'Kegiatan Bulanan';

    protected $form_view_tambahan = 'form_kinerja_bulanan_tambahan';

    protected $page_title_tambahan = 'Kegiatan Bulanan Tambahan';

    protected $index_view = 'index_kinerja_bulanan';

    protected function define_columns($grid){
       
       $grid->add('nama','Nama Kegiatan', true);
       $grid->add('bulan','Bulan')->cell( function( $value, $row) {
            return Util::get_bulan($row->bulan);
        });
       $grid->add('tahun','Tahun', true);
       $grid->add('jenis','Jenis Tugas'); 
       $grid->add('target_kuantitas','Target Kuantitas', true);
       $grid->add('target_waktu','Target waktu', true);
       $grid->add('target_kualitas','Target Kualitas (%)', true);
       $grid->add('capaian_kuantitas','Capaian Kuantitas', true);
       $grid->add('capaian_kualitas','Capaian Kualitas (%)', true);
       $grid->add('status','Status');

       $grid->edit('kinerja_bulanan/form', 'Edit','delete');
       
       $grid->row(function ($row) {         
        $row->cell('status')->value = $this->getStatusSign($row->data->status,$row->data->id);
        $row->cell('target_waktu')->value = $row->data->target_waktu." ".$row->data->nama_satuan_waktu;
        if($row->data->jenis ==  1){
          $row->cell('jenis')->value = "Tambahan";
          $row->cell('target_kuantitas')->value = "-";
          $row->cell('target_kualitas')->value = "-";
          $row->cell('capaian_kualitas')->value = "-";
          $row->cell('capaian_kuantitas')->value = "-";
		//$grid->edit('kinerja_bulanan/formtambahan', 'Edit','modify|delete');
		$edt_link = '<a class="" title="Edit" href="kinerja_bulanan/formtambahan?modify='.$row->data->id.'")><span class="fa fa-pencil-square-o"> </span></a>';

        } 
        else{
          $row->cell('jenis')->value = "Jabatan";
          $row->cell('target_kuantitas')->value = $row->data->target_kuantitas." ".$row->data->nama_satuan_jumlah;
          $row->cell('capaian_kuantitas')->value = $row->data->capaian_kuantitas." ".$row->data->nama_satuan_jumlah;
		$edt_link = '<a class="" title="Edit" href="kinerja_bulanan/form?modify='.$row->data->id.'")><span class="fa fa-pencil-square-o"> </span></a>';
       
	   }
        $row->cell('_edit')->value = $edt_link.'  '.$row->cell('_edit')->value;
        $new_link = '<a class="" title="Show" href="#" onclick=showDetail("/kinerja_bulanan/form?show=",'.$row->data->id.')><span class="glyphicon glyphicon-eye-open"> </span></a>';
        $row->cell('_edit')->value = $new_link.'  '.$row->cell('_edit')->value;

});
       //$grid->link('kinerja_bulanan/form',trans('rapyd::rapyd.add'), "TR");
    }

    protected function define_ordering($grid){
        $grid->orderBy('id','desc');
    }

    protected function define_fields($edit){       
      $tahun = Carbon::today()->format('Y');
      $user_id = $this->pegawai->getPegawaiId(); 
      if($edit->status != 'create'){
        $edit->add('kinerja_tahunan.tahun','Tahun', 'autocomplete')->mode('readonly');
      }
      if($edit->status == 'create'){
        $edit->set('pegawai_id', $user_id);
        $edit->set('tahun', $tahun);
        $edit->set('jenis', "0");
        $edit->set('satuan_target_waktu', 1);
      }
      $edit->add('nama','Nama Kegiatan', 'text')->rule('required');

      $edit->add('deskripsi','Deskripsi Kegiatan', 'textarea');

      $edit->add('bulan','Bulan','select')->options(Util::get_all_bulan());

      $edit->add('target_kuantitas','Target Kuantitas', 'text')->rule('required');

      $edit->add('satuan_target_kuantitas','<br/>','select')->options(SatuanJumlah::lists('nama', 'id')->all());

      $edit->add('target_waktu','Target Waktu', 'text')->rule('required');

      $edit->add('target_kualitas','Target Kualitas (%)', 'text')->rule('required');

      $edit->add('kinerja_tahunan','Kegiatan Tahunan','select')->options(KinerjaTahunan::where('tahun', $tahun)->where('pegawai_id', $user_id)->where('jenis', 0)->lists('nama', 'id'));
      
      if($edit->status == 'show'){
        $edit->text('capaian_kuantitas','Capaian Kuantitas')->mode('readonly');
        $edit->text('capaian_waktu','Capaian Waktu')->mode('readonly');
        $edit->text('capaian_kualitas','Capaian Kualitas (%)')->mode('readonly');
      }

      $edit->link("kinerja_bulanan",trans('rapyd::rapyd.back'))->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('kinerja_bulanan')->with('message', 'Success');
      });     
    }

    protected function define_fields_tambahan($edit){       
      $tahun = Carbon::today()->format('Y');
      $user_id = $this->pegawai->getPegawaiId(); 
      if($edit->status != 'create'){
        $edit->add('kinerja_tahunan.tahun','Tahun', 'autocomplete')->mode('readonly');
      }
      if($edit->status == 'create'){
        $edit->set('pegawai_id', $user_id);
        $edit->set('tahun', $tahun);
        $edit->set('jenis', 1);
      }
      $edit->add('nama','Nama Kegiatan', 'text')->rule('required');
      $edit->add('deskripsi','Deskripsi Kegiatan', 'textarea');
      $edit->add('bulan','Bulan','select')->options(Util::get_all_bulan());
      $edit->add('kinerja_tahunan','Kegiatan Tahunan','select')->options(KinerjaTahunan::where('tahun', $tahun)->where('pegawai_id', $user_id)->where('jenis', 1)->lists('nama', 'id'));
      $edit->add('target_waktu','Target Waktu', 'text')->rule('required');
      $edit->link("kinerja_bulanan",trans('rapyd::rapyd.back'))->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('kinerja_bulanan')->with('message', 'Success');
      });     
    }

    protected function define_filters($filter){
       $pegawai_id = $this->pegawai->getPegawaiId();
       $list_tahun = KinerjaTahunan::select('tahun')->where('pegawai_id', $pegawai_id)->distinct('tahun')->orderby('tahun', 'desc')->get();
       $array_opsi = array('' => 'Semua');
       foreach($list_tahun as $data_tahun){
          $array_opsi[$data_tahun->tahun] = $data_tahun->tahun;
       }
       $array_opsi_bulanan = array('' => 'Semua');
       $list_bulan = Util::get_all_bulan();
       foreach ($list_bulan as $id_bulan => $nama_bulan) {
         $array_opsi_bulanan[$id_bulan] = $nama_bulan;
       }
       $filter->add('nama','Nama', 'text');
       $filter->add('kinerja_tahunan.tahun','Tahun', 'select')->options($array_opsi);
       $filter->add('bulan','Bulan', 'select')->options($array_opsi_bulanan);
       $filter->submit('search');
       $filter->reset('reset');
    }

    protected function get_dataset(){
      $user_id = $this->pegawai->getPegawaiId(); 
      return KinerjaBulanan::select("kinerja_bulanan.*", "satuan_jumlah.nama as nama_satuan_jumlah", "satuan_waktu.nama as nama_satuan_waktu")
              ->leftJoin("satuan_jumlah", "kinerja_bulanan.satuan_target_kuantitas", "=", "satuan_jumlah.id")
              ->leftJoin("satuan_waktu", "kinerja_bulanan.satuan_target_waktu", "=", "satuan_waktu.id")
              ->where("pegawai_id", $user_id)
              ->orderBy("jenis", "ASC")
              ->orderBy("bulan", "ASC");
    }

    protected function get_extra_context(){
        $extra_context = parent::get_extra_context();
        $tahun = Carbon::today()->format('Y');
        $extra_context['tahun'] = $tahun;
        return $extra_context;
    }

    public function getGajibulanan(Request $request){
      $pegawai_id = $this->pegawai->getPegawaiId();
  
      $tahun = $request->get('tahun');
      if(!$tahun){
        $tahun = Carbon::today()->format('Y');
      }
  
      $bulan = $request->get('bulan');
      if(!$bulan){
        $bulan = Carbon::today()->format('m');
      }
      
      $arr_kinerja = array();

      $dataset = KinerjaBulanan::select('kinerja_bulanan.*', 'satuan_jumlah.nama as nama_jumlah', 'satuan_waktu.nama as nama_waktu', 'kinerja_bulanan.nama as nama_kegiatan', 'kinerja_bulanan.id as kinerja_id')
                  ->leftJoin('kinerja_tahunan', 'kinerja_tahunan.id', '=', 'kinerja_bulanan.kinerja_tahunan_id')
                  ->leftJoin('satuan_jumlah', 'kinerja_bulanan.satuan_target_kuantitas', '=', 'satuan_jumlah.id')
                  ->leftJoin('satuan_waktu', 'kinerja_bulanan.satuan_target_waktu', '=', 'satuan_waktu.id')
                  ->where('kinerja_tahunan.tahun', $tahun)
                  ->where('kinerja_bulanan.bulan', $bulan)
                  ->where('kinerja_bulanan.pegawai_id', $pegawai_id);

      foreach($dataset->get() as $kb){
        $presentase_capaiain = $kb->getPersentaseCapaianKuantitas();
        // Jika kegiatan jabatan
        if($kb->jenis == 0){
          $nilai_kinerja = $kb->getNilaiKinerja();
        }
        // Jika kegiatan tambahan
        if($kb->jenis == 1){
          $nilai_kinerja = 1;
        }
  
        $arr_kinerja[] = array(
        "id" => $kb->kinerja_id,
        "nama" => $kb->nama_kegiatan,
        "target_waktu" => $kb->target_waktu,
        "target_kuantitas" => $kb->target_kuantitas." ".$kb->nama_jumlah,
        "realisasi_kuantitas" => $kb->capaian_kuantitas." ".$kb->nama_jumlah.' ('.$presentase_capaiain.'%)',
        "realisasi_kualitas" => $kb->capaian_kualitas.'%',
        "realisasi_waktu" => $kb->capaian_waktu,
        "nilai_kinerja" => $nilai_kinerja,
        "jenis" => $kb->jenis
        );  
      }

      $skp = NilaiSKPBulanan::where([
          "pegawai_id" => $pegawai_id,
          "tahun" => $tahun,
          "bulan" => $bulan
        ])->first();
      
      if(!$skp){
        $skp = KinerjaBulanan::hitungNilaiSKPBulanan($bulan, $tahun, $pegawai_id);
      }
  
      //$nilai_skp = $nilai_tugas_jabatan + $nilai_tugas_tambahan;
  
      return View::make('ajax.tabel_gaji_bulanan')
                            ->with('array_kinerja' , $arr_kinerja)
                            ->with('nilai_tugas_jabatan' , $skp->nilai_tugas_jabatan)
                            ->with('nilai_tugas_tambahan' , $skp->nilai_tugas_tambahan)
                            ->with('nilai_skp' , $skp->nilai_skp);
                            //->with('bobot_jabatan', $bobot_jabatan)
                            //->with('tunjangan_dasar' , $tunjangan_dasar);
    }
}
