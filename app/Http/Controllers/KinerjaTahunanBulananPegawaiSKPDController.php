<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use App\SatuanJumlah;
use App\SatuanWaktu;
use App\KinerjaTahunan;
use App\KinerjaBulanan;
use App\Pegawai;
use Zofe\Rapyd\DataGrid;
use App\Util;

class KinerjaTahunanBulananPegawaiSKPDController extends KinerjaTahunanBulananController
{

  //protected $form_view = 'form_kinerja_peg_skpd';

  //protected $form_view_bulanan = 'form_kinerja_bulanan_peg_skpd';

  protected function define_fields($edit){    
    parent::define_fields($edit);
    $this->extra_context['nama_pegawai'] = $this->pegawai->nama;
    $this->extra_context['id_pegawai'] = $this->pegawai->getPegawaiId();
  }

  protected function define_fields_bulanan($edit, $id_tahunan=null){
    parent::define_fields_bulanan($edit, $id_tahunan);
    $this->extra_context['nama_pegawai'] = $this->pegawai->nama;
    $this->extra_context['id_pegawai'] = $this->pegawai->getPegawaiId();
  }

  public function getTahunanbulanan(Request $request){
    $pegawai_id = 0;
    if($request->input('pegawai_id')){
      $pegawai_id = $request->input('pegawai_id');
    }
    $pegawai = Pegawai::with('jabatan', 'instansi')->where('id', $pegawai_id)->get();
    if($pegawai->isEmpty()){
      return Response::make('Pegawai tidak ditemukan', 404);
    }
    // Kinerja tahunan/bulanan      
    $tahun = $request->get('tahun');
    $kinerja_tahunan = KinerjaTahunan::where('pegawai_id', $pegawai_id);
    $kinerja_bulanan = KinerjaBulanan::select('*', 'kinerja_bulanan.nama as nama_bulanan', 'kinerja_tahunan.nama as nama_tahunan', 'kinerja_bulanan.id as id_bulanan', 'kinerja_tahunan.id as id_tahunan', 'kinerja_bulanan.status as status_bulanan', 'kinerja_tahunan.status as status_tahunan')->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')->where('kinerja_tahunan.pegawai_id', $pegawai_id);
    if($tahun){
      $kinerja_tahunan = $kinerja_tahunan->where('tahun', $tahun);
      $kinerja_bulanan = $kinerja_bulanan->where('kinerja_tahunan.tahun', $tahun); 
    }
    $kinerja_tahunan = $kinerja_tahunan->get();
    $kinerja_bulanan = $kinerja_bulanan->get();

    $arr_kinerja = array();
    foreach($kinerja_tahunan as $kt){
      $arr_kinerja[] = array(
          "id" => $kt->id,
          "nama" => '<b><a href="#" onclick=showDetail("/keg_tahunan_bulanan_peg_skpd/form",'.$kt->id.')>'.$kt->nama.'</a></b>',
          "tahun_bulan" => "<b>".$kt->tahun."</b>",
          "status" => $this->getStatusSign($kt->status, 'tahunan_'.$kt->id),
          "action" => '<a class="btn btn-primary btn-xs" onclick=showInsertForm("/keg_tahunan_bulanan_peg_skpd/formbulanan/",'.$kt->id.',"pegawai_id='.$pegawai_id.'")><i class="fa fa-plus"/> Bulanan</a>
                            <a class="btn btn-success btn-xs" onclick=showUpdateForm("/keg_tahunan_bulanan_peg_skpd/form",'.$kt->id.')>Edit</a>
                            <a class="btn btn-danger btn-xs" onclick=showDeleteForm("/keg_tahunan_bulanan_peg_skpd/form",'.$kt->id.',"pegawai_id='.$pegawai_id.'")>Hapus</a>'
        );
      foreach($kinerja_bulanan as $kb){
        if($kb->kinerja_tahunan_id == $kt->id){
          $arr_kinerja[] = array(
          "id" => $kb->id_bulanan,
          "nama" => '<p style="padding-left : 15px"><a href="#" onclick=showDetail("/keg_tahunan_bulanan_peg_skpd/formbulanan",'.$kb->id_bulanan.')>'.$kb->nama_bulanan.'</a></p>',
          "tahun_bulan" => Util::get_bulan($kb->bulan),
          "status" => $this->getStatusSign($kb->status_bulanan, 'bulanan_'.$kb->id_bulanan),
          "action" => '<a class="btn btn-success btn-xs" onclick=showUpdateForm("/keg_tahunan_bulanan_peg_skpd/formbulanan",'.$kb->id_bulanan.')>Edit</a>
                            <a class="btn btn-danger btn-xs" onclick=showDeleteForm("/keg_tahunan_bulanan_peg_skpd/formbulanan",'.$kb->id_bulanan.',"pegawai_id='.$pegawai_id.'")>Hapus</a>'
          );
        }
      }
    }

    $grid = \DataGrid::source($arr_kinerja);
    $grid->paginate($this->pagination);
    $grid->add('nama','Nama Kegiatan');
    $grid->add('tahun_bulan','Tahun/Bulan');
    $grid->add('status','Status');
    $grid->add('action','Action');

    return View::make('ajax.tabel_tahunan_bulanan')
                      ->with('grid' , $grid);    
  }

  protected function checkFormPermission($edit){    
    if($edit->action == 'delete'){
      $pegawai_id = $this->request->input('pegawai_id');
      $this->pegawai = Pegawai::where('id',$pegawai_id)->first();
    }
    else{
      if($edit->status == 'create'){
        if($this->request->input('pegawai_id')){
          $pegawai_id = $this->request->input('pegawai_id');
          $this->pegawai = Pegawai::where('id',$pegawai_id)->first();
          if($this->pegawai==null){
            return false;
          }
        }
      }
      else{
        $pegawai_id = $edit->model->pegawai_id;
        $this->pegawai = Pegawai::where('id',$pegawai_id)->first();
        if($this->pegawai==null){
          return false;
        }
      }
    }
    return true;                
  }  

  protected function getRedirectUrl(){
    return 'peg_skpd/show/'.$this->pegawai->getPegawaiId().'?active=tahunan';
  }
  
}
