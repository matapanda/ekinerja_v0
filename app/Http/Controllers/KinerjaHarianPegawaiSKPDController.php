<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use App\KinerjaHarian;
use App\KinerjaBulanan;
use App\KinerjaTahunan;
use App\SatuanJumlah;
use App\Pegawai;

class KinerjaHarianPegawaiSKPDController extends KinerjaHarianController
{
    protected $form_view = 'form_kinerja_harian_peg_skpd';

    protected function define_fields($edit){
      parent::define_fields($edit);
      $this->extra_context['nama_pegawai'] = $this->pegawai->nama;
      $this->extra_context['id_pegawai'] = $this->pegawai->id;
    }

    function getTimelineharian(Request $request, $pegawai_id){
      $pegawai = Pegawai::with('jabatan', 'instansi')->where('id', $pegawai_id)->get();
      if($pegawai->isEmpty()){
        return Response::make('Pegawai tidak ditemukan', 404);
      }
      // Kinerja harian
      $kinerja_harian = KinerjaHarian::with('satuan_target_kuantitas')->where('pegawai_id', $pegawai_id)->orderBy('tanggal', 'desc');
      if($request->get('start')){
        $kinerja_harian = $kinerja_harian->where('tanggal','>=', $request->get('start'));
      }
      if($request->get('end')){
        $kinerja_harian = $kinerja_harian->where('tanggal','<=', $request->get('end'));
      }
      $kinerja_harian = $kinerja_harian->get();
      $tgl = "";
      $arr_kinerja = array();
      foreach ($kinerja_harian as $kinerja) {
        $date = $kinerja->tanggal->format('d M Y');
        if ( ! isSet($arr_kinerja[$date]) ) { 
            $arr_kinerja[$date] = array($kinerja);
        } else { 
            $arr_kinerja[$date][] = $kinerja;
        }
      }
      return View::make('ajax.timeline_harian_peg_skpd')
                          ->with('kinerja_harian' , $arr_kinerja);

    }

    protected function checkFormPermission($edit){    
        if($edit->action == 'delete'){
          $pegawai_id = $this->request->input('pegawai_id');
          $this->pegawai = Pegawai::where('id',$pegawai_id)->first();
        }
        else{
          if($edit->status == 'create'){
            if($this->request->input('pegawai_id')){
              $pegawai_id = $this->request->input('pegawai_id');
              $this->pegawai = Pegawai::where('id',$pegawai_id)->first();
              if($this->pegawai==null){
                return false;
              }
            }
          }
          else{
            $pegawai_id = $edit->model->pegawai_id;
            $this->pegawai = Pegawai::where('id',$pegawai_id)->first();
            if($this->pegawai==null){
              return false;
            }
          }
        }
        return true;                
    }  

    protected function getRedirectUrl(){
      return 'peg_skpd/show/'.$this->pegawai->getPegawaiId().'?active=harian';
    }
}
