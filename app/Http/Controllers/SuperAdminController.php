<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request as Req;

class SuperAdminController extends ModelController
{
    protected $user = null;
    public function __construct()
    { 
        if(Auth::user()){     
            if(!Auth::user()->isSuperAdmin()){
              return Redirect::to('/')->with('message', 'Halaman tidak boleh diakses')->send();
            }
        }
        else{
            return redirect()->guest('auth/login')->send();
        }
        $this->user =  Auth::user();
    }    
}