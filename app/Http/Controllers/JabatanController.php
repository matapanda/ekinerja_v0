<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\ModelController;
use Illuminate\Support\Facades\Redirect;

class JabatanController extends AdminController
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model = '\\App\\Jabatan';

    protected $page_title = 'Jabatan';

    protected function define_columns($grid){
       $grid->add('nama','Nama', true); 
       $grid->add('berwenang','Berwenang');
       $grid->edit('jabatan/form', 'Edit','show|modify|delete');     
       $grid->link('jabatan/form',trans('rapyd::rapyd.add'), "TR");
    }

    protected function define_ordering($grid){
        $grid->orderBy('id','asc');
    }

    protected function define_fields($edit){      
      $edit->add('nama','Nama Jabatan', 'text')->rule('required');
      $edit->add('berwenang','Berwenang?','checkbox');        
      $edit->link("jabatan","Kembali")->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('jabatan')->with('message', 'Success');
      });     
    }

    protected function define_filters($filter){
       $filter->add('nama','Nama', 'text');
       $filter->submit('search');
       $filter->reset('reset');
    }
}
