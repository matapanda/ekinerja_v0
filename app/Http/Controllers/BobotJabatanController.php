<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class BobotJabatanController extends SuperAdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model = '\\App\\BobotJabatan';

    protected $page_title = 'Bobot Jabatan';

    protected function define_columns($grid){
       $grid->add('nama','Nama', true); 
       $grid->add('nilai','Nilai Bobot', true); 
       $grid->edit('bobot_jabatan/form', 'Edit','show|modify|delete');     
       $grid->link('bobot_jabatan/form',trans('rapyd::rapyd.add'), "TR");
    }

    protected function define_ordering($grid){
        $grid->orderBy('id','asc');
    }

    protected function define_fields($edit){      
      $edit->add('nama','Nama Bobot Jabatan', 'text')->rule('required');
      $edit->add('nilai','Nilai', 'text')->rule('required');
      $edit->link("bobot_jabatan","Kembali")->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('bobot_jabatan')->with('message', 'Data berhasil disimpan');
      });     
    }

    protected function define_filters($filter){
       $filter->add('nama','Nama', 'text');
       $filter->submit('search');
       $filter->reset('reset');
    }
}
