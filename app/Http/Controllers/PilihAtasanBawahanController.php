<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Request as Req;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Util;
use DB;
use Zofe\Rapyd\DataForm;
use App\Pegawai;
use App\AtasanBawahan;
use Auth;
use Yajra\Datatables\Facades\Datatables;

class PilihAtasanBawahanController extends ModelController
{
    protected $model = '\\App\\AtasanBawahan';

    protected $page_title = 'Pegawai dan Atasan';

    protected $form_view = 'form';

    public function getIndex(){

    }

    public function getData(){
        $dataset = $this->get_dataset();
        return \Datatables::of($dataset)->make(true);
    }

    protected function define_columns($grid){
        $grid->add('{{ $nama_bawahan }}','Pegawai', true);
        $grid->add('{{ $nama_atasan }}','Atasan', true);
        $grid->edit('atasanbawahan/form', 'Edit','');
        $grid->row(function ($row) {
            $row->cell('_edit')->value = '<a class="" title="Edit" href="/atasanbawahan/form?pegawai_id='.$row->data->id_bawahan.'"><span class="glyphicon glyphicon-edit"> </span></a> ';
        });
    }

    public function anyForm(Request $request){
        $pegawai = $this->pegawai;
        $pegawai_id = $pegawai->getPegawaiId();
        
        $atasan_bawahan = AtasanBawahan::firstOrCreate(['bawahan_id' => $pegawai_id]);
        $edit = \DataForm::source($atasan_bawahan);
        $edit->add('bawahan_id', 'Pegawai','select')->options(array($pegawai_id => $pegawai->nama))->mode('readonly');
        
        $list_atasan = array();
        foreach($pegawai->getCalonAtasan() as $ats){
            $list_atasan[$ats->getPegawaiId()] = $ats->nama.' - '.$ats->nip_baru;
        }

        $edit->add('atasan_id', 'Atasan','select')->options($list_atasan);
        $edit->link($this->getRedirectUrl(),trans('rapyd::rapyd.back'))->back();
        $edit->saved(function () use ($edit) {        
            if(Input::has('pegawai_id')){                
                return Redirect::to($this->getRedirectUrl())->with('message' , 'Data atasan berhasil disimpan');
            }
            else{
                return Redirect::to('/')->with('message' , 'Data atasan berhasil disimpan');
            }
        }); 
        $page_activity = 'Ubah';
        $edit->submit('Simpan','BR');
        $result = $edit->view($this->form_view,compact('edit'))
              ->with('page_title',$this->page_title)
              ->with('page_activity',$page_activity)
              ->with('base_url','');     
        return $result;
    }

    /*protected function get_dataset(){
        $dataset = DB::table('atasan_bawahan')
                    ->select('bawahan.id_pegawai AS id_bawahan','bawahan.nama as nama_bawahan', 'atasan.nama as nama_atasan')
                    ->leftJoin('master.biodata as bawahan', 'atasan_bawahan.bawahan_id', '=', 'bawahan.pegawai_id')
                    ->leftJoin('master.biodata AS atasan', 'atasan_bawahan.atasan_id', '=', 'atasan.pegawai_id');
        /*
        if(Auth::user()->isAdmin()){            
            $dataset = $dataset->leftJoin('jabatan_terakhir', 'jabatan_terakhir.id_pegawai', '=', 'bawahan.id_pegawai')->leftJoin('posisi', 'jabatan_terakhir.posisi', '=', 'posisi.id_posisi');
            if($this->user->sub_unitk_skpd){
                $dataset = $dataset->where('posisi.sub_unitk', $this->user->sub_unitk_skpd);
            }
            if($this->user->unitk_skpd){
                $dataset = $dataset->where('posisi.unitk', $this->user->unitk_skpd);   
            }
            
        }
        return $dataset;
    }*/

	protected function get_dataset(){
        $dataset = DB::
                    connection('db_simpeg')
                    ->table('identitas_pegawai AS bawahan')
                    ->select('bawahan.id_pegawai AS id_bawahan','bawahan.nama as nama_bawahan', 'atasan.nama as nama_atasan', 'bawahan.nip_baru as nip_bawahan', 'atasan.nip_baru as nip_atasan')
                    ->leftJoin('ekinerja.atasan_bawahan', 'atasan_bawahan.bawahan_id', '=', 'bawahan.id_pegawai')
                    ->leftJoin('identitas_pegawai AS atasan', 'atasan_bawahan.atasan_id', '=', 'atasan.id_pegawai')
                    ->leftJoin('identitas_kepegawaian', 'bawahan.id_pegawai', '=', 'identitas_kepegawaian.id_pegawai')
                    ->whereIn('identitas_kepegawaian.status_pegawai', array(1,2));
        if(Auth::user()->isAdmin()){            
            $dataset = $dataset->leftJoin('jabatan_terakhir', 'jabatan_terakhir.id_pegawai', '=', 'bawahan.id_pegawai')->leftJoin('posisi', 'jabatan_terakhir.posisi', '=', 'posisi.id_posisi');
            if($this->user->sub_unitk_skpd){
               $dataset = $dataset->where('posisi.sub_unitk', $this->user->sub_unitk_skpd);
            }
            if($this->user->unitk_skpd){
                $dataset = $dataset->where('posisi.unitk', $this->user->unitk_skpd);   
            }
            
        }
        return $dataset;
    }




    protected function define_filters($filter){
        /*
       $filter->add('bawahan.nama','Nama Pegawai', 'text');
       $filter->submit('search');
       $filter->reset('reset');
       */
    }

    protected function getRedirectUrl(){
      return '/';
    }
}
