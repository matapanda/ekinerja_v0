<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Request as Req;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\KinerjaBulanan;
use App\KinerjaTahunan;
use App\KinerjaHarian;
use App\Util;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class ReportController extends Controller
{
    protected $pegawai_id = null;

    protected $pegawai = null;

    public function __construct()
    {
        $this->middleware('auth');
        if(!Auth::user()){
          if (Request::ajax()) {
              return response('Halaman tidak berhak diakses.', 401);
          } else {
              return redirect()->guest('auth/login');
          }
        }
        $this->pegawai = Util::getPegawai();
        $this->pegawai_id = $this->pegawai->getPegawaiId();
        View::share('pegawai_global', $this->pegawai);
    }
    
    //
    public function getIndex()
    {
        $tahun=date("Y");
        $bulan=Util::get_bulan(date("n"));
        $extra_context = array(
            "tahun" => $tahun,
            "bulan" => $bulan, 
            "jumlah_kegiatan_tahunan" => $this->getJumlahKegiatanTahunan()
        );
        return View::make('chart_report')->with($extra_context);
    }

    public function getJumlahKegiatanTahunan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;
        $jumlah = DB::table('kinerja_tahunan')
                ->where('pegawai_id', $pegawai_id)
                ->where('tahun', $tahun)
                ->count('id');
        
        return $jumlah;
    }

    public function getJumlahKegiatanBulanan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;
        $bulan=date("n");

        $jumlah = KinerjaBulanan::select(DB::raw('count(kinerja_bulanan.id) as jumlah'))
                ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
                ->where('kinerja_bulanan.pegawai_id', $pegawai_id)
                ->where('kinerja_tahunan.tahun', $tahun)
                ->where('bulan', $bulan)
                ->first();

        return $jumlah->jumlah;
    }

    public function getCapaianKuantitasTahunan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;
        $capaian_kuantitas_tahunan = KinerjaTahunan::select(DB::raw('sum(capaian_kuantitas) as total_capaian_kuantitas'), DB::raw('sum(target_kuantitas) as total_target_kuantitas'))
                ->groupBy('tahun')
                ->where('tahun', $tahun)
                ->where('pegawai_id', $pegawai_id)
                ->first();

        $persentase_capaian_kuantitas = $capaian_kuantitas_tahunan->total_capaian_kuantitas*100/$capaian_kuantitas_tahunan->total_target_kuantitas;
        return number_format($persentase_capaian_kuantitas, 2, '.', '');
    }

    public function getCapaianKualitasTahunan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;
        $capaian_kualitas_tahunan = KinerjaTahunan::select(DB::raw('sum(capaian_kualitas) as total_capaian_kualitas'), DB::raw('sum(target_kualitas) as total_target_kualitas'))
                ->groupBy('tahun')
                ->where('tahun', $tahun)
                ->where('pegawai_id', $pegawai_id)
                ->first();

        $persentase_capaian_kualitas = $capaian_kualitas_tahunan->total_capaian_kualitas*100/$capaian_kualitas_tahunan->total_target_kualitas;
        return number_format($persentase_capaian_kualitas, 2, '.', '');
    }

    public function getKegiatanBulanan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;
        $bulan=date("m");
        $kinerja_bulanan =KinerjaBulanan::select('*', 'kinerja_bulanan.nama as nama', 'satuan_waktu.nama as nama_satuan_waktu', 'satuan_jumlah.nama as nama_satuan_jumlah')
                        ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
                        ->leftJoin('satuan_waktu', 'kinerja_bulanan.satuan_target_waktu', '=', 'satuan_waktu.id')
                        ->leftJoin('satuan_jumlah', 'kinerja_bulanan.satuan_target_kuantitas', '=', 'satuan_jumlah.id')
                        ->where('kinerja_bulanan.pegawai_id', $pegawai_id)
                        ->where('kinerja_tahunan.tahun', $tahun)
                        ->where('bulan', $bulan)
                        ->orderby('kinerja_bulanan.id', 'desc')
                        ->get();

        return $kinerja_bulanan;
    }

    public function getLaporanJumlahBulanan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;
        
        $laporan_kinerja_bulanan = KinerjaBulanan::select('bulan', 'kinerja_tahunan.tahun as tahun', DB::raw('count(kinerja_bulanan.id) as total'))
                ->groupBy('tahun')
                ->groupBy('bulan')
                ->orderby('tahun', 'desc')
                ->orderby('bulan', 'desc')
                ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
                ->where('kinerja_bulanan.pegawai_id', $pegawai_id)
                ->get();

        return $laporan_kinerja_bulanan;    
    }

    public function getLaporanCapaianBulanan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;

        $laporan_kinerja_bulanan = KinerjaBulanan::select('bulan', 'kinerja_tahunan.tahun as tahun', DB::raw('sum(kinerja_bulanan.target_kuantitas) as total_target_kuantitas'), DB::raw('sum(kinerja_bulanan.capaian_kuantitas) as total_capaian_kuantitas'), DB::raw('sum(kinerja_bulanan.target_kualitas) as total_target_kualitas'), DB::raw('sum(kinerja_bulanan.capaian_kualitas) as total_capaian_kualitas'), DB::raw('sum(kinerja_bulanan.target_waktu) as total_target_waktu'), DB::raw('sum(kinerja_bulanan.capaian_waktu) as total_capaian_waktu'))
                ->groupBy('tahun')
                ->groupBy('bulan')
                ->orderby('tahun', 'desc')
                ->orderby('bulan', 'desc')
                ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
                ->where('kinerja_bulanan.pegawai_id', $pegawai_id)
                ->get();

        return $laporan_kinerja_bulanan;    
    }

    public function getLaporan(Request $request)
    {
        $tipe = Input::get('tipe');
        $tahun = Input::get('tahun');
        $id_peg = Input::get('id_peg');

        if ($tahun==0) $tahun = date("Y");
        if($id_peg==0) $id_peg=$this->pegawai_id;

        if ($tipe==1) return $this->getLaporanKegiatanHarian($tahun, $id_peg);
        else if ($tipe==2) return $this->getKualitasKegiatanBulanan($tahun, $id_peg);
        else if ($tipe==3) return $this->getLaporanKuantitasBulanan($tahun, $id_peg);
        else return $this->getLaporanJamKerjaBulanan($tahun, $id_peg);
    }

    public function getLaporanKegiatanHarian($tahun, $id_peg)
    {
        $status_diterima_bulanan=0;
        $status_diterima_harian=2;
        $laporan_kinerja_harian = KinerjaHarian::select(DB::raw('count(kinerja_harian.id) as nilai'), DB::raw('YEAR(tanggal) tahun, MONTH(tanggal) bulan'))
                ->leftJoin('kinerja_bulanan', 'kinerja_bulanan_id', '=', 'kinerja_bulanan.id')
                ->where('kinerja_bulanan.pegawai_id', $id_peg)
                ->where('kinerja_bulanan.status', $status_diterima_bulanan)
                ->where('kinerja_harian.status', $status_diterima_harian)
                ->whereYear('tanggal','=', $tahun)
                ->groupBy('tahun')
                ->groupBy('bulan')
                ->orderby('tahun', 'asc')
                ->orderby('bulan', 'asc')
                ->get();
        
        $hasil ='[';
        $list_bulan = array( 1=>"Jan", 2=>"Feb", 3=>"Mar", 4=>"Apr",  5=>"Mei", 6=>"Jun", 7=>"Jul", 8=>"Agu", 9=>"Sept", 10=>"Okt", 11=>"Nov", 12=>"Des");
        for($i=1; $i<=12; $i++){
            if ($i>1) $hasil=$hasil.',';

            $temp='{"nilai":0,"tahun":'.$tahun.',"bulan":'.$i.', "nama_bulan" : "'.$list_bulan[$i].'"}';
                foreach($laporan_kinerja_harian as $r){
                    if ($i==$r->bulan) $temp='{"nilai":'.$r->nilai.',"tahun":'.$r->tahun.',"bulan":'.$r->bulan.', "nama_bulan" : "'.$list_bulan[$i].'"}';
                }
            $hasil=$hasil.''.$temp;
        }
        $hasil=$hasil.']';
        
        return $hasil;
    }

    public function getKualitasKegiatanBulanan($tahun, $id_peg)
    {
        $status_diterima_bulanan=0;
        $status_diterima_harian=2;

        $kualitas_kinerja_harian = KinerjaBulanan::select(DB::raw('AVG(kinerja_bulanan.capaian_kualitas) as nilai'), 'bulan')
                ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
                ->where('kinerja_bulanan.pegawai_id', $id_peg)
                ->where('kinerja_bulanan.status', $status_diterima_bulanan)
                ->where('kinerja_tahunan.tahun', $tahun)
                ->groupBy('kinerja_tahunan.tahun')
                ->groupBy('bulan')
                ->orderby('kinerja_tahunan.tahun', 'asc')
                ->orderby('bulan', 'asc')
                ->get();
        
        $hasil ='[';
        $list_bulan = array( 1=>"Jan", 2=>"Feb", 3=>"Mar", 4=>"Apr",  5=>"Mei", 6=>"Jun", 7=>"Jul", 8=>"Agu", 9=>"Sept", 10=>"Okt", 11=>"Nov", 12=>"Des");
        for($i=1; $i<=12; $i++){
            if ($i>1) $hasil=$hasil.',';

            $temp='{"nilai":0,"bulan":'.$i.', "nama_bulan" : "'.$list_bulan[$i].'"}';
                foreach($kualitas_kinerja_harian as $r){
                    if ($i==$r->bulan) $temp='{"nilai":'.$r->nilai.',"bulan":'.$r->bulan.', "nama_bulan" : "'.$list_bulan[$i].'"}';
                }
            $hasil=$hasil.''.$temp;
        }
        $hasil=$hasil.']';
        return $hasil;
    }

    public function getLaporanKuantitasBulanan($tahun, $id_peg)
    {
        $status_diterima_bulanan=0;
        $status_diterima_harian=2;

        $laporan_kuantitas_bulanan = KinerjaHarian::select(DB::raw('SUM(kinerja_harian.capaian_kuantitas)*100/kinerja_bulanan.target_kuantitas as nilai'), DB::raw('YEAR(tanggal) tahun, MONTH(tanggal) bulan'))
                ->leftJoin('kinerja_bulanan', 'kinerja_bulanan_id', '=', 'kinerja_bulanan.id')
                ->where('kinerja_bulanan.pegawai_id', $id_peg)
                ->where('kinerja_bulanan.status', $status_diterima_bulanan)
                ->where('kinerja_harian.status', $status_diterima_harian)
                ->whereYear('tanggal','=', $tahun)
                ->groupBy('tahun')
                ->groupBy('bulan')
                ->orderby('tahun', 'asc')
                ->orderby('bulan', 'asc')
                ->get();
        
        $hasil ='[';
        $list_bulan = array( 1=>"Jan", 2=>"Feb", 3=>"Mar", 4=>"Apr",  5=>"Mei", 6=>"Jun", 7=>"Jul", 8=>"Agu", 9=>"Sept", 10=>"Okt", 11=>"Nov", 12=>"Des");
        for($i=1; $i<=12; $i++){
            if ($i>1) $hasil=$hasil.',';

            $temp='{"nilai":0,"tahun":'.$tahun.',"bulan":'.$i.', "nama_bulan" : "'.$list_bulan[$i].'"}';
                foreach($laporan_kuantitas_bulanan as $r){
                    if ($i==$r->bulan) $temp='{"nilai":'.$r->nilai.',"tahun":'.$r->tahun.',"bulan":'.$r->bulan.', "nama_bulan" : "'.$list_bulan[$i].'"}';
                }
            $hasil=$hasil.''.$temp;
        }
        $hasil=$hasil.']';
        
        return $hasil;
    }

    public function getLaporanJamKerjaBulanan($tahun, $id_peg)
    {
        $status_diterima_bulanan=0;
        $status_diterima_harian=2;

        $laporan_jam_kerja = KinerjaHarian::select(DB::raw('SUM((TIME_TO_SEC(waktu_akhir) - TIME_TO_SEC(waktu_awal))/3600) as nilai'), DB::raw('YEAR(tanggal) tahun, MONTH(tanggal) bulan'))
                ->leftJoin('kinerja_bulanan', 'kinerja_bulanan_id', '=', 'kinerja_bulanan.id')
                ->where('kinerja_bulanan.pegawai_id', $id_peg)
                ->where('kinerja_bulanan.status', $status_diterima_bulanan)
                ->where('kinerja_harian.status', $status_diterima_harian)
                ->whereYear('tanggal','=', $tahun)
                ->groupBy('tahun')
                ->groupBy('bulan')
                ->orderby('tahun', 'asc')
                ->orderby('bulan', 'asc')
                ->get();
        
        $hasil ='[';
        $list_bulan = array( 1=>"Jan", 2=>"Feb", 3=>"Mar", 4=>"Apr",  5=>"Mei", 6=>"Jun", 7=>"Jul", 8=>"Agu", 9=>"Sept", 10=>"Okt", 11=>"Nov", 12=>"Des");
        for($i=1; $i<=12; $i++){
            if ($i>1) $hasil=$hasil.',';

            $temp='{"nilai":0,"tahun":'.$tahun.',"bulan":'.$i.', "nama_bulan" : "'.$list_bulan[$i].'"}';
                foreach($laporan_jam_kerja as $r){
                    if ($i==$r->bulan) $temp='{"nilai":'.$r->nilai.',"tahun":'.$r->tahun.',"bulan":'.$r->bulan.', "nama_bulan" : "'.$list_bulan[$i].'"}';
                }
            $hasil=$hasil.''.$temp;
        }
        $hasil=$hasil.']';
        
        return $hasil;
    }

    public function getTunjangan(){
      $pegawai_id = $this->pegawai_id;
      $list_tahun = KinerjaTahunan::select('tahun')->where('pegawai_id', $pegawai_id)->distinct('tahun')->orderby('tahun', 'desc')->lists('tahun', 'tahun');
      $list_bulan = Util::get_all_bulan();
      $selected_bulan = Carbon::today()->format('m');
      return View::make('index_gaji')
                          ->with('list_tahun' , $list_tahun)
                          ->with('list_bulan' , $list_bulan)
                          ->with('selected_bulan' , $selected_bulan)
                          ->with('page_title','Perolehan Tunjangan Kinerja')
                          ->with('page_activity','Perolehan Tunjangan Kinerja')
                          ->with('base_url',action(class_basename($this)."@getIndex"));  
   }

}
