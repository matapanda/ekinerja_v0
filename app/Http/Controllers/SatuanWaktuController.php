<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class SatuanWaktuController extends SuperAdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model = '\\App\\SatuanWaktu';

    protected $page_title = 'Satuan Waktu';

    protected function define_columns($grid){
       $grid->add('nama','Nama', true); 
       $grid->edit('satuan_waktu/form', 'Edit','show|modify|delete');     
       $grid->link('satuan_waktu/form',trans('rapyd::rapyd.add'), "TR");
    }

    protected function define_ordering($grid){
        $grid->orderBy('id','asc');
    }

    protected function define_fields($edit){      
      $edit->add('nama','Nama Satuan Waktu', 'text')->rule('required');
      $edit->link("satuan_waktu","Kembali")->back();

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('satuan_waktu')->with('message', 'Data berhasil disimpan');
      });     
    }

    protected function define_filters($filter){
       $filter->add('nama','Nama', 'text');
       $filter->submit('search');
       $filter->reset('reset');
    }
}
