<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Instansi;
use App\Jabatan;
use App\Pegawai;
use App\KinerjaHarian;
use App\KinerjaTahunan;
use App\KinerjaBulanan;
use Zofe\Rapyd\DataGrid;
use App\Util;

class PegawaiSKPDController extends BawahanController
{

    protected $page_title = 'Pegawai SKPD';

    protected $form_view = 'pegawai_skpd_profile';

    public function __construct()
    {
        parent::__construct();       
    }

    public function getShow(Request $request, $pegawai_id){
      $active = 'tahunan';
      if( $request->input('active') ){
        $act = $request->input('active');
        if($act == 'harian'){
          $active = 'harian';
        }
      }
      $this->extra_context['active'] = $active;
      return parent::getShow($request, $pegawai_id);
    }

    protected function define_columns($grid){
       $grid->add('nip','NIP', true);
       $grid->add('nama','Nama', true); 
       $grid->row(function ($row) {
         $new_link = '<a class="" title="Detail" href="peg_skpd/show/'.$row->data->id.'">'.$row->data->nama.'</a>';
         $row->cell('nama')->value = $new_link;
       });
       $grid->add('{{ $jabatan->nama }}','Jabatan'); 
       $grid->add('{{ $instansi->nama }}','Instansi');
    }

    protected function get_dataset(){
        $obj = new $this->model;
        $obj = $obj::with('jabatan','instansi');
        $instansi = $this->pegawai->instansi;
        $instansi_bawahan = $instansi->getInstansiSKPD();
        return $obj->whereIn('instansi_id', $instansi_bawahan);        
    }
}
