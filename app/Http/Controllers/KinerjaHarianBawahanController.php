<?php

namespace App\Http\Controllers;

use Request as Req;
use Illuminate\Http\Request;

use Zofe\Rapyd\DataForm;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Response as FacadeResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\KinerjaHarian;
use App\KinerjaBulanan;
use App\KinerjaTahunan;
use App\SatuanJumlah;
use App\Pegawai;
use App\KomentarKinerjaHarian;
use App\Util;
use App\Notifikasi;
use App\IdentitasPegawai;
use App\AtasanBawahan;
use DB;
use Log;

class KinerjaHarianBawahanController extends KinerjaHarianController
{
    protected $page_title = "Kinerja Harian Bawahan";

    public function getIndex(){
      $atasan = AtasanBawahan::where('bawahan_id', $this->pegawai->getPegawaiId())->first();
      if(!$atasan){
        return Redirect::to('atasanbawahan/form');
      }
      
      $list_pegawai = array('' => 'Semua Pegawai');
        foreach($this->pegawai->getBawahan()->get() as $bwh){
          $list_pegawai[$bwh->id_pegawai] = $bwh->nip_baru.' - '.$bwh->nama;
        }

      $list_status = array("" => "Semua Status") + Util::getStatus();

      $selected_status = "";
      if(Input::get('status')){
        $selected_status = Input::get('status');        
      }
      $tanggal_awal = "";
      $tanggal_akhir = "";
      if(!Input::get('semuatanggal')){
        $tanggal_sekarang = Carbon::today();
        $tanggal_akhir = $tanggal_sekarang->endOfMonth()->toDateString();
        $tanggal_awal = $tanggal_sekarang->startOfMonth()->toDateString();
      }

      return view('index_kinerja_harian_bawahan')
                ->with('page_title',$this->page_title)
                ->with('base_url',action(class_basename($this)."@getIndex"))
                ->with('list_pegawai',$list_pegawai)
                ->with('list_status',$list_status)
                ->with('selected_status',$selected_status)
                ->with('tanggal_awal',$tanggal_awal)
                ->with('tanggal_akhir',$tanggal_akhir);
    }
    protected function define_fields($edit){
      if($edit->status == 'show'){   
        $edit->add('nama','Nama Kegiatan', 'text')->rule('required');
        $edit->add('deskripsi','Deskripsi Kegiatan', 'redactor');
       
        $edit->add('tanggal','Tanggal', 'date')->format('d M Y', 'en', 'd M Y')->mode('readonly');      

        $edit->add('waktu_awal','Waktu Awal', 'text')->rule('required');
        $edit->add('waktu_akhir','Waktu Akhir', 'text')->rule('required');

        $edit->add('capaian_kuantitas','Capaian Kuantitas', 'text')->rule('required');

        $edit->add('satuan_target_kuantitas','<br/>','select')->options(SatuanJumlah::lists('nama', 'id')->all());

        $edit->add('kinerja_bulanan','Kegiatan Bulanan','select')->options(KinerjaBulanan::all()->lists('nama', 'id'))->rule('required');
        //$edit->add('bukti','Bukti', 'file');

        $list_komentar = KomentarKinerjaHarian::where('kinerja_harian_id', $edit->model->id)->get();
        $this->extra_context['list_komentar'] = $list_komentar; 
      }
      else{
        $edit->add('status','Status Kegiatan', 'hidden');
      }
    }

    function getTimelineharian(Request $request, $type = 'timeline'){      
      // Kinerja harian
      /*
      $kinerja_harian = KinerjaHarian::with('satuan_target_kuantitas','identitas_pegawai')->orderBy('tanggal', 'desc');
      */


      $kinerja_harian = KinerjaHarian::
          select('kinerja_harian.*',
            DB::Raw('IFNULL(satuan_jumlah.nama, "") as nama_satuan_jumlah'),
            DB::Raw('IFNULL(kinerja_bulanan.nama, "Tugas Tambahan") as nama_kegiatan_bulanan'))
          ->leftJoin('kinerja_bulanan', 'kinerja_bulanan.id', '=', 'kinerja_harian.kinerja_bulanan_id')
          ->leftJoin('satuan_jumlah', 'satuan_jumlah.id','=', 'kinerja_harian.satuan_target_kuantitas')
          //->leftJoin('simpeg.identitas_pegawai as identitas_pegawai', 'identitas_pegawai.id_pegawai', '=', 'kinerja_harian.pegawai_id')
          ->orderBy('tanggal','DESC');

      // Filter pegawai bawahan saja
      $list_bawahan = AtasanBawahan::where("atasan_id", $this->pegawai->getPegawaiId())->get();
      if(count($list_bawahan) > 0){
        $arr_bawahan = array();
          foreach($list_bawahan as $bw){
              $arr_bawahan[] =  $bw->bawahan_id;
          }
          $kinerja_harian = $kinerja_harian->whereIn('kinerja_harian.pegawai_id', $arr_bawahan);
      }
        
      // Filter by request
      if($request->get('pegawai_id')){
        $pegawai_id = $request->get('pegawai_id');
        /*
        $pegawai = IdentitasPegawai::where('id_pegawai', $pegawai_id)->get();
        if(!$pegawai){
          return FacadeResponse::make('Pegawai tidak ditemukan', 404);
        }        */
        $kinerja_harian = $kinerja_harian->where('kinerja_harian.pegawai_id', $pegawai_id);
      }
      else{
        
      }

      if($request->get('start')){
        $kinerja_harian = $kinerja_harian->where('kinerja_harian.tanggal','>=', $request->get('start'));
      }
      if($request->get('end')){
        $kinerja_harian = $kinerja_harian->where('kinerja_harian.tanggal','<=', $request->get('end'));
      }
      if($request->get('status')){
        $kinerja_harian = $kinerja_harian->where('kinerja_harian.status', $request->get('status'));
      }
      if($request->get('kinerja_bulanan')){
        $kinerja_harian = $kinerja_harian->where('kinerja_harian.kinerja_bulanan_id', $request->get('kinerja_bulanan'));
      }
      $kinerja_harian = $kinerja_harian->get();
      
      $arr_kinerja = array();
      foreach ($kinerja_harian as $kinerja) {
        $date = $kinerja->tanggal->format('d M Y');
        if ( ! isSet($arr_kinerja[$date]) ) { 
            $arr_kinerja[$date] = array($kinerja);
        } else { 
            $arr_kinerja[$date][] = $kinerja;
        }
      }

      $view = 'ajax.timeline_harian';
      if($type != 'timeline'){
        $view = 'ajax.tabel_harian';
      }
      return View::make($view)
                          ->with('kinerja_harian' , $arr_kinerja);

    }

    function anyAccept(Request $request){
      $edit = \DataEdit::source(new KinerjaHarian);
      if(!$this->checkFormPermission($edit)){
            if(Req::ajax()){
                return response('Halaman tidak berhak diakses.', 401);
            }
            else{
                return Redirect::to($this->getRedirectUrl())->with('message', 'Halaman tidak berhak diakses');
            }
        }
      $edit->add('status','Status Kegiatan', 'hidden');
      $edit->add('capaian_kualitas','Capaian Kualitas', 'text');
      $extra_context = array("action" => "accept");
      return $this->changestatus($request, $edit, $extra_context, "accept");
    }

    function anyReject(Request $request){
      $edit = \DataEdit::source(new KinerjaHarian);
      if(!$this->checkFormPermission($edit)){
            if(Req::ajax()){
                return response('Halaman tidak berhak diakses.', 401);
            }
            else{
                return Redirect::to($this->getRedirectUrl())->with('message', 'Halaman tidak berhak diakses');
            }
        }
      $edit->add('status','Status Kegiatan', 'hidden');
      $extra_context = array("action" => "reject");
      return $this->changestatus($request, $edit, $extra_context, "reject");
    }

    function changestatus(Request $request, $edit, $extra_context, $action){
      
      $edit->saved(function ($action) use ($edit, $request) {
        if($edit->model->kinerja_bulanan_id){
          $id_bulanan = $edit->model->kinerja_bulanan_id;            
       
          $kinerja_bulanan = KinerjaBulanan::where('id', $id_bulanan)->first();

          if($kinerja_bulanan){
            $kinerja_bulanan->hitungKinerja();
          }
        }

        // Hitung ulang notifikasi sebagai atasan
        Notifikasi::hitungNotifikasiSebagaiAtasan($this->pegawai->getPegawaiId());
        // Hitung ulang notifikasi pegawai
        Notifikasi::hitungNotifikasiSebagaiPegawai($edit->model->pegawai_id);

        //Insert komentar baru
        $now = Carbon::now('Asia/Jakarta');
        KomentarKinerjaHarian::insert([
            'status' => $edit->model->status,
            'kinerja_harian_id' => $edit->model->id,
            'komentar' => $request->get('komentar'),
            'created_at' => $now
          ]);
      }); 
      
      $edit->build();
      // If untuk menampilkan form
      if($edit->action == 'idle'){    
        $form_view = 'ajax.form_konfirmasi_ubah_status';
      }
      // Else, kerjakan action dan cek statusnya
      else{
        if($edit->process_status == 'success'){
          return response()->json(array("status"=>$edit->process_status));
        }
        else{
          return response()->json(array("status"=>$edit->process_status),400);
        }
      }      
      $result = $edit->view($form_view, compact('edit'))->with($extra_context);     
      return $result;
    }

  protected function checkFormPermission($edit){
    if($edit->status == 'modify' || $edit->status == 'delete' || $edit->status == 'show'){
      $kinerja = KinerjaHarian::where('id', $edit->model->id)->first();
      if(!$kinerja){
        return false;
      }
      else{
        if(!$this->pegawai->isAtasan($kinerja->pegawai_id)){
          return false;
        }
      }
    }    
    return true;
  }
}
