<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\IdentitasPegawai;
use App\Util;

class TestController extends Controller
{
    public function getIndex(){
        $pegawai = IdentitasPegawai::where('id_pegawai',1)->first();
        $bawahan_langsung = $pegawai->getBawahanLangsung()->get();
        foreach($bawahan_langsung as $bawahan){
            echo $bawahan->nama." ".$bawahan->keterangan_posisi." ".$bawahan->keterangan_unitk."<br/>";
        }
        echo $pegawai->getUnitK()->keterangan;
        echo "<br/>";
        echo $pegawai->getEselon()->keterangan;
    }

    public function getCalonatasan(){
        $pegawai = IdentitasPegawai::where('id_pegawai',3101)->first();
        $calon_atasan = $pegawai->getCalonAtasan()->get();
        foreach($calon_atasan as $atasan){
            echo $atasan->nama." ".$atasan->keterangan_posisi." ".$atasan->keterangan_unitk."<br/>";
        }
    }

    public function getAtasanbawahan(){


    }

    public function getPegawai(){
        echo Util::getPegawaiId();
    }
}
