<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Request as Req;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\KinerjaBulanan;
use App\KinerjaTahunan;
use App\KinerjaHarian;
use App\SatuanJumlah;
use App\KomentarKinerjaHarian;
use Session;
use App\Util;
use App\Notifikasi;
use App\Pegawai;
use DB;
use Log;
use App\AtasanBawahan;
use Yajra\Datatables\Facades\Datatables;

class KinerjaHarianController extends KinerjaController
{
    protected $pegawai_id = null;

    protected $model = '\\App\\KinerjaHarian';

    protected $page_title = 'Kegiatan Harian';

    protected $form_view = 'form_kinerja_harian';

    protected $page_title_tambahan = 'Kegiatan Tambahan';

    protected $form_view_tambahan = 'form_kinerja_harian_tambahan';

    public function __construct(Req $request)
    {
        parent::__construct();       
    }

    public function getIndex()
    {
      $list_status = array("" => "Semua Status") + Util::getStatus();

      $selected_status = "";
      if(Input::get('status')){
        $selected_status = Input::get('status');        
      }

       $index_view = 'index_datatables';

       return view($index_view)
                ->with('list_status',$list_status)
                ->with('page_title',$this->page_title)
                ->with('selected_status',$selected_status)
                ->with('base_url',action(class_basename($this)."@getIndex"));
    }

    public function getData()
    {
      $query = $this->get_dataset();
      $datatables = Datatables::of($query)
        ->addColumn('tanggal', function($kinerja) {
          return Carbon::createFromFormat('Y-m-d H:i:s', $kinerja->tanggal)->format('d-m-Y');
        })
        ->addColumn('waktu', function($kinerja) {
          return $kinerja->waktu_awal." - ".$kinerja->waktu_akhir;
        })
        ->addColumn('capaian_kuantitas', function($kinerja) {
          return $kinerja->capaian_kuantitas." ".$kinerja->nama_satuan_jumlah;
        })
        ->addColumn('nama_kegiatan_bulanan', function($kinerja) {
          return $kinerja->nama_kegiatan_bulanan;
        })
        ->addColumn('jenis', function($kinerja) {
          if($kinerja->jenis == 1){
            return "Tambahan";
          }
          else{
            return "Jabatan";
          }
        })
        ->addColumn('status', function($kinerja) {
          return $this->getStatusSign($kinerja->status,$kinerja->id);
        })
        ->addColumn('action', function($kinerja) {
              $new_link = '';
              if($kinerja->status != Util::STATUS_SETUJU){
						if($kinerja->kinerja_bulanan_id != 0){
						  if($kinerja->jenis != 0){
							  $new_link = '<a class="" title="Edit" href="/kinerja_harian/formtambahan?update='.$kinerja->id.'"><span class="glyphicon glyphicon-edit"> </span></a> ';
						  }else{
							$new_link = '<a class="" title="Edit" href="/kinerja_harian/form?update='.$kinerja->id.'"><span class="glyphicon glyphicon-edit"> </span></a> ';  
						  }
                  $new_link = $new_link.'<a class="text-danger" title="Delete" href="/kinerja_harian/form?delete='.$kinerja->id.'"><span class="glyphicon glyphicon-trash"> </span></a> ';
				}
                else
				{
                  $new_link = '<a class="" title="Edit" href="/kinerja_harian/formtambahan?update='.$kinerja->id.'"><span class="glyphicon glyphicon-edit"> </span></a> ';
                  $new_link = $new_link.'<a class="text-danger" title="Delete" href="/kinerja_harian/formtambahan?delete='.$kinerja->id.'"><span class="glyphicon glyphicon-trash"> </span></a> ';
                }
              }
              if($kinerja->jenis == 1){
                $new_link = '<a class="" title="Show" href="#" onclick=showDetail("/kinerja_harian/formtambahan?show=",'.$kinerja->id.')><span class="glyphicon glyphicon-eye-open"> </span></a> '.$new_link;
              }
              else{
                $new_link = '<a class="" title="Show" href="#" onclick=showDetail("/kinerja_harian/form?show=",'.$kinerja->id.')><span class="glyphicon glyphicon-eye-open"> </span></a> '.$new_link;
              }
              return $new_link;
            }
        );

        if ($status = $datatables->request->get('status')) {
            $datatables->where('kinerja_harian.status', $status);
        }

        if ($nama = $datatables->request->get('nama')) {
            $datatables->where('kinerja_harian.nama', 'like', "$nama%");
        }

        return $datatables->make(true);

    }

    public function anyForm(Request $request){
     $atasan = AtasanBawahan::where('bawahan_id', $this->pegawai->getPegawaiId())->first();
      if(!$atasan){
        return Redirect::to('atasanbawahan/form');
      }
      return parent::anyForm($request);
    }

    protected function define_columns($grid){       
       $grid->add('nama','Nama Kegiatan', true);
       $grid->add('tanggal|strtotime|date[d M Y]','Tanggal', true);
       $grid->add('{{ $waktu_awal." - ".$waktu_akhir }}','Waktu', true);
       //$grid->add('waktu_akhir','Waktu Akhir', true);
       $grid->add('{{ $capaian_kuantitas." ".$nama_satuan_jumlah }}','Capaian Kuantitas');
       $grid->add('{{ $nama_kegiatan_bulanan }}','Kegiatan Bulanan');
       $grid->add('status','Status'); 
       $grid->row(function ($row) {         
         $row->cell('status')->value = $this->getStatusSign($row->data->status,$row->data->id);
       });
       $grid->edit('kinerja_harian/form', 'Edit','');
       $grid->row(function ($row) {
       $new_link = '';
        if($row->data->status != Util::STATUS_SETUJU){
          $new_link = '<a class="" title="Edit" href="/kinerja_harian/form?update='.$row->data->id.'"><span class="glyphicon glyphicon-edit"> </span></a> ';
          $new_link = $new_link.'<a class="text-danger" title="Delete" href="/kinerja_harian/form?delete='.$row->data->id.'"><span class="glyphicon glyphicon-trash"> </span></a> ';
        }
          $new_link = '<a class="" title="Show" href="#" onclick=showDetail("/kinerja_harian/form?show=",'.$row->data->id.')><span class="glyphicon glyphicon-eye-open"> </span></a> '.$new_link;
          $row->cell('_edit')->value = $new_link.'  '.$row->cell('_edit')->value;
       });   
       $grid->link('kinerja_harian/form',trans('rapyd::rapyd.add'), "TR");
       $grid->link('kinerja_harian/formtambahan',trans('Tambah Kegiatan Tambahan'), "TR");
    }

    protected function define_fields($edit){       
      $user_id = $this->pegawai->getPegawaiId();
      $bulan = Carbon::now()->month;
      $tanggal = Carbon::today()->format('Y-m-d');
      $tahun = Carbon::today()->format('Y');
      $arr = array();
      $is_insert = false;
      
      $edit->add('nama','Nama Kegiatan', 'text')->rule('required')->extraAttributes($arr);
      $edit->add('deskripsi','Deskripsi Kegiatan', 'textarea');
      
      
      if($edit->status == 'create'){
        //$edit->set('tanggal', $tanggal);
        $edit->set('pegawai_id', $user_id);
        $is_insert =  true;
      }
      else{
        $is_insert =  false;
        //$edit->add('tanggal','Tanggal', 'date')->format('d M Y', 'en', 'd M Y')->mode('readonly');
      }
      //$edit->add('tanggal','Tanggal', 'date')->format('d-m-Y', 'id','Y-m-d')->rule('date_valid')->rule('required');
     // $edit->add('waktu_awal','Waktu Awal', 'datetime')->format('H:i:s', 'id', 'H:i:s');
      //$edit->add('waktu_akhir','Waktu Akhir', 'datetime')->format('H:i:s', 'id', 'H:i:s');

      //$edit->add('tanggal','Tanggal', 'date')->format('d-m-Y', 'id','Y-m-d')->rule('date_valid')->rule('required');
      //Set Batasan Tanggal disini
	  $edit->add('tanggal','Tanggal', 'date')->format('d-m-Y', 'id','Y-m-d')->rule('required');
      $edit->add('waktu_awal','Waktu Awal', 'text')->rule('required')->rule('time_valid');
      $edit->add('waktu_akhir','Waktu Akhir', 'text')->rule('required')->rule('time_valid');

      $edit->add('capaian_kuantitas','Capaian Kuantitas', 'text')->rule('required');

      $edit->add('satuan_target_kuantitas','<br/>','hidden');

      /*
      $edit->add('kinerja_bulanan','Kegiatan Bulanan','select')->options(KinerjaBulanan::leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')->where('bulan', $bulan)->where('kinerja_tahunan.pegawai_id', $user_id)->lists('kinerja_bulanan.nama', 'kinerja_bulanan.id'))->rule('required');
      */
      if($edit->status == 'show'){
        $list_bulanan = KinerjaBulanan::select('*', 'satuan_jumlah.nama as nama_jumlah', 'satuan_waktu.nama as nama_waktu', 'kinerja_bulanan.nama as nama_kegiatan', 'kinerja_bulanan.id as kinerja_id', 'satuan_jumlah.id as id_jumlah', 'satuan_waktu.id as id_waktu')
            ->where('bulan', $bulan)
            ->where('pegawai_id', $user_id)
            ->where('kinerja_bulanan.jenis', "0")
            ->leftJoin('satuan_jumlah', 'satuan_target_kuantitas', '=', 'satuan_jumlah.id')
            ->leftJoin('satuan_waktu', 'satuan_target_waktu', '=', 'satuan_waktu.id')
            ->get();
      }
      else{
        $list_bulanan = KinerjaBulanan::select('*', 'satuan_jumlah.nama as nama_jumlah', 'satuan_waktu.nama as nama_waktu', 'kinerja_bulanan.nama as nama_kegiatan', 'kinerja_bulanan.id as kinerja_id', 'satuan_jumlah.id as id_jumlah', 'satuan_waktu.id as id_waktu', 'kinerja_tahunan.id as id_kinerja_tahunan')
            ->where('bulan', $bulan)
            ->where('kinerja_tahunan.pegawai_id', $user_id)
            ->where('kinerja_bulanan.tahun', $tahun)
            ->where('kinerja_bulanan.jenis', "0")
            ->leftJoin('satuan_jumlah', 'satuan_target_kuantitas', '=', 'satuan_jumlah.id')
            ->leftJoin('satuan_waktu', 'satuan_target_waktu', '=', 'satuan_waktu.id')
            ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
            ->get();
        //$edit->add('status', '', 'auto')->updateValue('1');
        $edit->set('status', Util::STATUS_BELUM_DIPERIKSA);
        $edit->set('jenis', "0");
      }
      
      $array_bulanan = array();
      $array_opsi = array();
      foreach($list_bulanan as $bulanan){
        $array_bulanan[$bulanan->kinerja_id] = $bulanan;
        $array_opsi[$bulanan->kinerja_id] = $bulanan->nama_kegiatan;
      }
      $this->extra_context['json_kinerja_bulanan'] = json_encode($array_bulanan);
      $edit->add('kinerja_bulanan','Kegiatan Bulanan','select')->options($array_opsi)->rule('required');

      if(Input::hasFile('bukti')){
        $ekstensi = Input::file('bukti')->getClientOriginalExtension();
      }
      else{
        $ekstensi = NULL;
      }
      $nama_file = $user_id.'_'.Carbon::now()->format('Ymd_His');
      $edit->add('bukti','Bukti', 'file')->move('ubk',$nama_file.'.'.$ekstensi);

      if($edit->status == 'show'){
        $list_komentar = KomentarKinerjaHarian::where('kinerja_harian_id', $edit->model->id)->get();
        $this->extra_context['list_komentar'] = $list_komentar; 
      }

      $selected_kinerja_bulanan_id = 0;
      if($edit->status == 'modify'){
        $selected_kinerja_bulanan_id = $edit->model->kinerja_bulanan_id;
      }
      $this->extra_context['selected_kinerja_bulanan_id'] = $selected_kinerja_bulanan_id; 

      $edit->link($this->getRedirectUrl(),trans('rapyd::rapyd.back'))->back();

      if($edit->status == 'create'){
        $edit->saved(function () use ($edit) {
          $atasan_id = $this->pegawai->getAtasanLangsungId();
          if($atasan_id){
            //Notifikasi::tambahNotifikasiSebagaiAtasan($atasan_id,1);
            Notifikasi::hitungNotifikasiSebagaiAtasan($atasan_id);
          }
          return Redirect::to($this->getRedirectUrl())->with('message' , 'Data berhasil disimpan');
        });
      }
      else{
        $edit->saved(function () use ($edit) {
          return Redirect::to($this->getRedirectUrl())->with('message' , 'Data berhasil disimpan');
        });
      }
    }

    protected function define_filters($filter){
       $filter->add('nama','Nama', 'text');
       //$filter->add('tanggal','tanggal','daterange')->format('Y-m-d', 'en');
       $array_opsi = array("" => "Semua")+Util::getStatus();
       $filter->add('status','Kegiatan Bulanan','select')->options($array_opsi);
       $filter->submit('search');
       $filter->reset('reset');
    }

    protected function get_dataset(){
        $user_id = $this->pegawai->getPegawaiId(); 
        //$user_id="10350";
        $kinerja_harian = KinerjaHarian::
          select('kinerja_harian.*',
            DB::Raw('IFNULL(satuan_jumlah.nama, "") as nama_satuan_jumlah'),
            DB::Raw('IFNULL(kinerja_bulanan.nama, "Tugas Tambahan") as nama_kegiatan_bulanan'))
          ->leftJoin('kinerja_bulanan', 'kinerja_bulanan.id', '=', 'kinerja_harian.kinerja_bulanan_id')
          ->leftJoin('satuan_jumlah', 'satuan_jumlah.id','=', 'kinerja_harian.satuan_target_kuantitas')
          ->where('kinerja_harian.pegawai_id',$user_id)
          ->orderBy('kinerja_harian.jenis', 'ASC')
          ->orderBy('tanggal','DESC');
        return $kinerja_harian;
          
        //return $obj::with('satuan_target_kuantitas','kinerja_bulanan')->where('pegawai_id', $user_id)->orderBy('tanggal','DESC');
    }

    protected function get_extra_context(){
        $extra_context = parent::get_extra_context();
        $tanggal = Carbon::today()->format('d M Y');
        $extra_context['tanggal'] = $tanggal;
        return $extra_context;
    }

    protected function checkFormPermission($edit){
      if($edit->status == 'modify' || $edit->status == 'delete' || $edit->status == 'show'){
        $kinerja = KinerjaHarian::where('id', $edit->model->id)->first();
        if(!$kinerja){
          return false;
        }
        else{
          if($kinerja->pegawai_id != $this->pegawai->getPegawaiId()){
            return false;
          }
          if($edit->status == 'modify' || $edit->status == 'delete'){
            if($edit->model->status == Util::STATUS_SETUJU){
              return false;
            }
          }
        }
      }    
      return true;
    }

    protected function getRedirectUrl(){
      return 'kinerja_harian';
    }

    public function getDaftarkinerjabulanan(){
      if(Input::has("tanggal")){
        $input_tanggal = Input::get("tanggal");
        $tanggal = Carbon::createFromFormat('d-m-Y', $input_tanggal);
      }
      else{
        $tanggal = Carbon::today();
      }
      $bulan = $tanggal->month;
      $tahun = $tanggal->year;
      
      $list_bulanan = KinerjaBulanan::select('satuan_jumlah.nama as nama_jumlah', 'satuan_waktu.nama as nama_waktu', 'kinerja_bulanan.nama as nama_kegiatan', 'kinerja_bulanan.id as kinerja_id', 'satuan_jumlah.id as id_jumlah', 'satuan_waktu.id as id_waktu', 'kinerja_tahunan.id as id_kinerja_tahunan')
          ->where('bulan', $bulan)
		  
          ->where('kinerja_tahunan.pegawai_id', $this->pegawai->getPegawaiId())
          ->where('kinerja_tahunan.tahun', $tahun)
		  
          ->leftJoin('satuan_jumlah', 'satuan_target_kuantitas', '=', 'satuan_jumlah.id')
          ->leftJoin('satuan_waktu', 'satuan_target_waktu', '=', 'satuan_waktu.id')
          ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id');

      if(Input::has("tambahan")){
        $list_bulanan = $list_bulanan->where("kinerja_bulanan.jenis", 1);
      }else{
		  $list_bulanan = $list_bulanan->where("kinerja_bulanan.jenis", 0);
		  }

      $list_bulanan = $list_bulanan->get();
        
      $array_bulanan = array();
      foreach($list_bulanan as $bulanan){
        $array_bulanan[$bulanan->kinerja_id] = $bulanan;
      }
      return response()->json($array_bulanan);      
    }

    protected function define_fields_tambahan($edit){
      $user_id = $this->pegawai->getPegawaiId();
      $bulan = Carbon::now()->month;
      $tanggal = Carbon::today()->format('Y-m-d');
      $tahun = Carbon::today()->format('Y');
      $arr = array();
      
      $edit->add('nama','Nama Kegiatan', 'text')->rule('required')->extraAttributes($arr);
      $edit->add('deskripsi','Deskripsi Kegiatan', 'textarea');
      
      
      if($edit->status == 'create'){
        $edit->set('pegawai_id', $user_id);
      }

      $edit->set('kinerja_bulanan_id', '0');
      $edit->set('satuan_target_kuantitas', '0');
     
// FILTER TANGGAL KEGAITAN TAMBAHAN 
   //$edit->add('tanggal','Tanggal', 'date')->format('d-m-Y', 'id','Y-m-d')->rule('date_valid')->rule('required');
      
	$edit->add('tanggal','Tanggal', 'date')->format('d-m-Y', 'id','Y-m-d')->rule('required');
      $edit->add('waktu_awal','Waktu Awal', 'text')->rule('required')->rule('time_valid');
      $edit->add('waktu_akhir','Waktu Akhir', 'text')->rule('required')->rule('time_valid');

         
      if($edit->status == 'show'){
        $list_bulanan = KinerjaBulanan::select('*', 'satuan_jumlah.nama as nama_jumlah', 'satuan_waktu.nama as nama_waktu', 'kinerja_bulanan.nama as nama_kegiatan', 'kinerja_bulanan.id as kinerja_id', 'satuan_jumlah.id as id_jumlah', 'satuan_waktu.id as id_waktu')
            ->where('bulan', $bulan)
            ->where('pegawai_id', $user_id)
            ->where('kinerja_bulanan.jenis', 1)
            ->leftJoin('satuan_jumlah', 'satuan_target_kuantitas', '=', 'satuan_jumlah.id')
            ->leftJoin('satuan_waktu', 'satuan_target_waktu', '=', 'satuan_waktu.id')
            ->get();
      }
      else{
        $list_bulanan = KinerjaBulanan::select('*', 'satuan_jumlah.nama as nama_jumlah', 'satuan_waktu.nama as nama_waktu', 'kinerja_bulanan.nama as nama_kegiatan', 'kinerja_bulanan.id as kinerja_id', 'satuan_jumlah.id as id_jumlah', 'satuan_waktu.id as id_waktu', 'kinerja_tahunan.id as id_kinerja_tahunan')
            ->where('bulan', $bulan)
            ->where('kinerja_tahunan.pegawai_id', $user_id)
            ->where('kinerja_bulanan.tahun', $tahun)
            ->where('kinerja_bulanan.jenis', 1)
            ->leftJoin('satuan_jumlah', 'satuan_target_kuantitas', '=', 'satuan_jumlah.id')
            ->leftJoin('satuan_waktu', 'satuan_target_waktu', '=', 'satuan_waktu.id')
            ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
            ->get();
        //$edit->add('status', '', 'auto')->updateValue('1');
        $edit->set('status', Util::STATUS_BELUM_DIPERIKSA);
        $edit->set('jenis', 1);
      }
      
      $array_bulanan = array();
      $array_opsi = array();
      foreach($list_bulanan as $bulanan){
        $array_bulanan[$bulanan->kinerja_id] = $bulanan;
        $array_opsi[$bulanan->kinerja_id] = $bulanan->nama_kegiatan;
      }
      $this->extra_context['json_kinerja_bulanan'] = json_encode($array_bulanan);
      $edit->add('kinerja_bulanan','Kegiatan Bulanan','select')->options($array_opsi)->rule('required');
      
    
      if(Input::hasFile('bukti')){
        $ekstensi = Input::file('bukti')->getClientOriginalExtension();
      }
      else{
        $ekstensi = NULL;
      }
      $nama_file = $user_id.'_'.Carbon::now()->format('Ymd_His');
      $edit->add('bukti','Bukti', 'file')->move('ubk',$nama_file.'.'.$ekstensi);

      if($edit->status == 'show'){
        $list_komentar = KomentarKinerjaHarian::where('kinerja_harian_id', $edit->model->id)->get();
        $this->extra_context['list_komentar'] = $list_komentar; 
      }

    
      $edit->link($this->getRedirectUrl(),trans('rapyd::rapyd.back'))->back();

      $edit->saved(function () use ($edit) {
        $atasan = $this->pegawai->getAtasanLangsung();
        if($atasan){
          $jumlah_notif = Notifikasi::hitungNotifikasi($atasan);
        }
        return Redirect::to($this->getRedirectUrl())->with('message' , 'Data berhasil disimpan');
      });
    }
}
