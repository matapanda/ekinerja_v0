<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Request as Req;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use Util;
use DB;
use Zofe\Rapyd\DataForm;
use App\User;
use Auth;
use Yajra\Datatables\Facades\Datatables;

class UserController extends AdminController
{
    protected $model = '\\App\\User';

    protected $page_title = 'User';

    protected $form_view = 'form';

    public function getIndex()
    {
        $index_view = 'admin.index_user';
        return view($index_view)
                ->with('list_atasan', $this->get_dataset()->get())
                ->with('page_title',$this->page_title)
                ->with('base_url',action(class_basename($this)."@getIndex"));
    }

    public function getData(){
        $dataset = $this->get_dataset();
        $datatables = Datatables::of($dataset)
            ->addColumn('username', function($user) {
            return $user->username;
            })
            ->addColumn('action', function($user) {
                $new_link = '<a class="" title="Edit" href="/user/changepassword/'.$user->id.'"><span class="glyphicon glyphicon-edit"> </span></a> ';
                return $new_link;
              }
          );
        
        if ($username = $datatables->request->get('username')) {
            $datatables->where('username', 'like', "%$username%");
        }
        $count = $datatables->count();
        return $datatables
                    ->with([
                        "recordsTotal"    => $count,
                        "recordsFiltered" => $count,
                    ])
                    ->make(true);
    }

    protected function define_columns($grid){
        $grid->add('{{ $username }}','Pegawai', true);
        $grid->edit('user/form', 'Edit','');
        $grid->row(function ($row) {
            $row->cell('_edit')->value = '<a class="" title="Edit" href="/atasanbawahan/form?pegawai_id='.$row->data->id_bawahan.'"><span class="glyphicon glyphicon-edit"> </span></a> ';
        });
    }

    public function getChangepassword($id_user){
        $user = User::where("id",$id_user)->first();
        if(!$user){
            return Redirect::to($this->getRedirectUrl())->with('message' , 'User tidak ditemukan');
        }
        return view('admin.form_change_password')
                ->with('user', $user)
                ->with('page_title',$this->page_title)
                ->with('page_activity', 'Ubah Password')
                ->with('base_url',action(class_basename($this)."@getIndex"));
    }

    public function postChangepassword(Request $request, $id_user){
        $user = User::where("id",$id_user)->first();
        if(!$user){
            return Redirect::to($this->getRedirectUrl())->with('message' , 'User tidak ditemukan');
        }
        $user->password = md5(Input::get('password'));
        $user->save();
        return Redirect::to('user/changepassword/'.$id_user)->with('message' , 'Password berhasil diubah');
    }

    protected function get_dataset(){
        return User::where("activated","=","1");
    }

    protected function getRedirectUrl(){
      return 'user';
    }
}
