<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\KinerjaBulanan;
use App\KinerjaTahunan;
use App\Util;
use App\AtasanBawahan;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    protected $pegawai_id = null;

    protected $pegawai = null;

    public function __construct()
    {
        Log::debug("Hello");
        Log::debug(Auth::user());
        $this->middleware('auth');
        if(!Auth::user()){
          Log::debug("Not auth");
          if (Request::ajax()) {
              return response('Halaman tidak berhak diakses.', 401);
          } else {
              return redirect()->guest('auth/login');
          }
        }
        /*
        if(Auth::user()->banned == 1)
        {
            Auth::logout();
            return redirect()->guest('auth/login')
                ->withErrors("Akun anda tidak bisa digunakan");
        }*/
        if(Auth::user()->hasRole('user')){
            $this->pegawai = Util::getPegawai();
            $this->pegawai_id = $this->pegawai->getPegawaiId();
            View::share('pegawai_global', $this->pegawai);
        }
    }
    //
    public function getIndex()
    {        
        Log::debug("Masuk index dashboard");
        if(Auth::user()->hasRole('user')){
            $atasan = AtasanBawahan::where('bawahan_id', $this->pegawai->getPegawaiId())->first();
            if(!$atasan){
                return Redirect::to('pilih_atasan');
            }
            else{
                if($atasan->atasan_id == 0){
                    return Redirect::to('pilih_atasan');       
                }
            }
            $tahun=date("Y");
            $bulan=Util::get_bulan(date("n"));
            $extra_context = array(
                "tahun" => $tahun,
                "bulan" => $bulan, 
                "jumlah_kegiatan_tahunan" => $this->getJumlahKegiatanTahunan()
            );
            return View::make('chart')->with($extra_context);
        }
        else{
            return View::make('admin.chart');
        }        
    }

    public function getStatistik(){
        $jumlah_kegiatan_tahunan = $this->getJumlahKegiatanTahunan();
        $jumlah_kegiatan_bulanan = $this->getJumlahKegiatanBulanan();
        $statistik = array(
                "jumlah_kegiatan_tahunan" => $jumlah_kegiatan_tahunan,
                "jumlah_kegiatan_bulanan" => $jumlah_kegiatan_bulanan
            );        
        return json_encode($statistik);
    }

    public function getJumlahKegiatanTahunan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai->getPegawaiId();
        $jumlah = DB::table('kinerja_tahunan')
                ->where('pegawai_id', $pegawai_id)
                ->where('tahun', $tahun)
                ->count('id');
        
        return $jumlah;
    }

    public function getJumlahkegiatanbulanan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai->getPegawaiId();
        $bulan=date("n");
        $jumlah = KinerjaBulanan::select(DB::raw('count(kinerja_bulanan.id) as jumlah'))
                ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
                ->where('kinerja_bulanan.pegawai_id', $pegawai_id)
                ->where('kinerja_tahunan.tahun', $tahun)
                ->where('bulan', $bulan)
                ->first();
        return $jumlah->jumlah;
    }

    public function getCapaianKuantitasTahunan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;
        $capaian_kuantitas_tahunan = KinerjaTahunan::select(DB::raw('sum(capaian_kuantitas) as total_capaian_kuantitas'), DB::raw('sum(target_kuantitas) as total_target_kuantitas'))
                ->groupBy('tahun')
                ->where('tahun', $tahun)
                ->where('pegawai_id', $pegawai_id)
                ->first();

        $persentase_capaian_kuantitas = $capaian_kuantitas_tahunan->total_capaian_kuantitas*100/$capaian_kuantitas_tahunan->total_target_kuantitas;
        return number_format($persentase_capaian_kuantitas, 2, '.', '');
    }

    public function getCapaianKualitasTahunan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;
        $capaian_kualitas_tahunan = KinerjaTahunan::select(DB::raw('sum(capaian_kualitas) as total_capaian_kualitas'), DB::raw('sum(target_kualitas) as total_target_kualitas'))
                ->groupBy('tahun')
                ->where('tahun', $tahun)
                ->where('pegawai_id', $pegawai_id)
                ->first();

        $persentase_capaian_kualitas = $capaian_kualitas_tahunan->total_capaian_kualitas*100/$capaian_kualitas_tahunan->total_target_kualitas;
        return number_format($persentase_capaian_kualitas, 2, '.', '');
    }

    public function getKegiatanBulanan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;
        $bulan=date("m");
        $kinerja_bulanan =KinerjaBulanan::select('*', 'kinerja_bulanan.nama as nama', 'satuan_waktu.nama as nama_satuan_waktu', 'satuan_jumlah.nama as nama_satuan_jumlah')
                        ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
                        ->leftJoin('satuan_waktu', 'kinerja_bulanan.satuan_target_waktu', '=', 'satuan_waktu.id')
                        ->leftJoin('satuan_jumlah', 'kinerja_bulanan.satuan_target_kuantitas', '=', 'satuan_jumlah.id')
                        ->where('kinerja_bulanan.pegawai_id', $pegawai_id)
                        ->where('kinerja_tahunan.tahun', $tahun)
                        ->where('bulan', $bulan)
                        ->orderby('kinerja_bulanan.id', 'desc')
                        ->get();

        return $kinerja_bulanan;
    }

    public function getLaporanJumlahBulanan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;
        
        $laporan_kinerja_bulanan = KinerjaBulanan::select('bulan', 'kinerja_tahunan.tahun as tahun', DB::raw('count(kinerja_bulanan.id) as total'))
                ->groupBy('tahun')
                ->groupBy('bulan')
                ->orderby('tahun', 'desc')
                ->orderby('bulan', 'desc')
                ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
                ->where('kinerja_bulanan.pegawai_id', $pegawai_id)
                ->get();

        return $laporan_kinerja_bulanan;    
    }

    public function getLaporanCapaianBulanan()
    {
        $tahun=date("Y");
        $pegawai_id=$this->pegawai_id;

        $laporan_kinerja_bulanan = KinerjaBulanan::select('bulan', 'kinerja_tahunan.tahun as tahun', DB::raw('sum(kinerja_bulanan.target_kuantitas) as total_target_kuantitas'), DB::raw('sum(kinerja_bulanan.capaian_kuantitas) as total_capaian_kuantitas'), DB::raw('sum(kinerja_bulanan.target_kualitas) as total_target_kualitas'), DB::raw('sum(kinerja_bulanan.capaian_kualitas) as total_capaian_kualitas'), DB::raw('sum(kinerja_bulanan.target_waktu) as total_target_waktu'), DB::raw('sum(kinerja_bulanan.capaian_waktu) as total_capaian_waktu'))
                ->groupBy('tahun')
                ->groupBy('bulan')
                ->orderby('tahun', 'desc')
                ->orderby('bulan', 'desc')
                ->leftJoin('kinerja_tahunan', 'kinerja_tahunan_id', '=', 'kinerja_tahunan.id')
                ->where('kinerja_bulanan.pegawai_id', $pegawai_id)
                ->get();

        return $laporan_kinerja_bulanan;    
    }

}
