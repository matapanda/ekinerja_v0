<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ModelController;
use App\Instansi;
use Illuminate\Support\Facades\Redirect;
use Zofe\Rapyd\DataTree;
use Zofe\Rapyd\DataGrid;

class InstansiNewController extends AdminController
{
    protected $model = '\\App\\Instansi';

    protected $str = "";

    protected $arr = array();

    protected $page_title = 'Instansi';

    public function getIndex()
    {                
        $arr = Instansi::getNestedList('nama', null, '---');
        $data = array();
        foreach ($arr as $key => $value){
          //$data[] = array('id' => $key, 'nama' => $value, );
          $row = new \stdClass();
          $row->id = $key;
          $row->nama = $value;
          $data[] = $row;
        }
        $tree = \DataGrid::source($data);
        $tree->add('nama', 'Nama');
        $tree->edit("instansi/form", 'Edit', 'show|modify|delete', 'id');
        $tree->link('instansi/form',trans('rapyd::rapyd.add'), "TR");
        //$tree->submit('Save the order');
        $index_view = (isset($this->index_view) ? 
            $this->index_view : 'index-tree');

       return view($index_view, compact('tree'));
    }

    protected function define_columns($grid){
       $grid->add('nama','Nama', true); 
       $grid->edit('instansi/form', 'Edit','show|modify|delete');     
       $grid->link('instansi/form',trans('rapyd::rapyd.add'), "TR");
    }

    protected function define_fields($edit){      
      $edit->add('nama','Nama Instansi', 'text')
            ->rule('required');
      
      $this->arr = array("" => "Induk");
      //$tree = Instansi::get()->toTree();
      //$arr = $arr + $this->traverse($tree);
      //$this->traverse($tree);
      $this->arr = $this->arr + Instansi::getNestedList('nama', null, '---');
      $edit->add('parent_id','Instansi Induk','select')->options($this->arr);

      $edit->saved(function () use ($edit) {
        $edit->message("Record saved");
        return Redirect::to('instansi')->with('message', 'Success');
      });     
    }

    protected function traverse($tree, $prefix='--'){
        foreach ($tree as $el) {
            $this->arr[$el->id] = $prefix.' '.$el->nama;

            $this->traverse($el->children, $prefix.'-');
        }
        //return $arr;
    }
}
