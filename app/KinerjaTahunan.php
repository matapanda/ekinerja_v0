<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KinerjaTahunan extends Model
{
    //
    protected $table = 'kinerja_tahunan';

    public $timestamps = false;

    protected $fillable   = ['nama', 'pegawai_id', 'tahun', 'target_kuantitas', 'satuan_target_kuantitas', 'target_waktu', 'satuan_target_waktu', 'target_kualitas', 'status_target', 'capaian_kuantitas', 'capaian_waktu', 'capaian_kualitas', 'status_capaian', 'skor', 'status'];

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'pegawai_id');
    }

    public function satuan_target_kuantitas()
    {
        return $this->belongsTo('App\SatuanJumlah', 'satuan_target_kuantitas');
    }

    public function satuan_target_waktu()
    {
        return $this->belongsTo('App\SatuanWaktu', 'satuan_target_waktu');
    }

    public function kinerja_bulanan()
    {
        return $this->hasMany('App\KinerjaBulanan');
    }
}
