<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\KinerjaHarian;
use App\Util;
use App\Pegawai;
use App\AtasanBawahan;
use Log;

class Notifikasi extends Model
{
    //
    protected $table = 'notifikasi';

    public $timestamps = false;

    protected $fillable = ['pegawai_id', 'notifikasi_atasan', 'notifikasi_pegawai'];

    public static function hitungNotifikasiSebagaiPegawai($pegawai_id){
    	$jumlah_notifikasi = KinerjaHarian::where('pegawai_id', $pegawai_id)->where('status', Util::STATUS_TOLAK)->count();
    	$notif = Notifikasi::firstOrNew(["pegawai_id" => $pegawai_id]);
        $notif->notifikasi_pegawai = $jumlah_notifikasi;
        $notif->pegawai_id = $pegawai_id;
        $notif->save();
        return $jumlah_notifikasi;
    }

    public static function tambahNotifikasiSebagaiPegawai($pegawai_id, $jumlah=1){
        $notif = Notifikasi::firstOrNew(["pegawai_id" => $pegawai_id]);
        $notif->notifikasi_pegawai = $notif->notifikasi_pegawai + $jumlah;
        $notif->pegawai_id = $pegawai_id;
        $notif->save();
    }

    public static function kurangiNotifikasiSebagaiPegawai($pegawai_id, $jumlah=1){
        $notif = Notifikasi::firstOrNew(["pegawai_id" => $pegawai_id]);
        if($jumlah > $notif->notifikasi_pegawai){
            $notif->notifikasi_pegawai = 0;
        }
        else{
            $notif->notifikasi_pegawai = $notif->notifikasi_pegawai - $jumlah;
        }
        $notif->pegawai_id = $pegawai_id;
        $notif->save();
    }

    public static function getNotifikasiSebagaiPegawai($pegawai_id){
    	$notif = Notifikasi::where('pegawai_id', $pegawai_id)->first();
    	if(!$notif){
    		$notif = Notifikasi::hitungNotifikasi($pegawai_id);
            return $notif->notifikasi_pegawai;
    	}
    	else{
    		return $notif->notifikasi_pegawai;
    	}
    }

    public static function hitungNotifikasiSebagaiAtasan($pegawai_id){
        $list_bawahan = AtasanBawahan::where('atasan_id', "=", $pegawai_id)->get();
        
    	$arr_bawahan = array();
        foreach($list_bawahan as $bw){
            array_push($arr_bawahan, $bw->bawahan_id);
        }
        
        if(count($arr_bawahan) == 0){
            return 0;
        }

        $jumlah_notifikasi = KinerjaHarian::whereIn('pegawai_id', $arr_bawahan)->where('status', Util::STATUS_BELUM_DIPERIKSA)->count();
        $notif = Notifikasi::firstOrNew(["pegawai_id" => $pegawai_id]);
        $notif->notifikasi_atasan = $jumlah_notifikasi;
        $notif->pegawai_id = $pegawai_id;
        $notif->save();
        return $jumlah_notifikasi;
    }

    public static function tambahNotifikasiSebagaiAtasan($pegawai_id, $jumlah=1){
        $notif = Notifikasi::firstOrNew(["pegawai_id" => $pegawai_id]);
        $notif->notifikasi_atasan = $notif->notifikasi_atasan + $jumlah;
        $notif->pegawai_id = $pegawai_id;
        $notif->save();
        return $notif->notifikasi_atasan;
    }

    public static function kurangiNotifikasiSebagaiAtasan($pegawai_id, $jumlah=1){
        $notif = Notifikasi::firstOrNew(["pegawai_id" => $pegawai_id]);
        if($jumlah > $notif->notifikasi_atasan){
            $notif->notifikasi_atasan = 0;
        }
        else{
            $notif->notifikasi_atasan = $notif->notifikasi_atasan - $jumlah;
        }
        $notif->pegawai_id = $pegawai_id;
        $notif->save();
        return $notif->notifikasi_atasan;
    }

    public static function getNotifikasiSebagaiAtasan($pegawai_id){
    	$notif = Notifikasi::where('pegawai_id', $pegawai_id)->first();
    	if(!$notif){
    		$notif = Notifikasi::hitungNotifikasi($pegawai_id);
            return $notif->notifikasi_atasan;
    	}
    	else{
    		return $notif->notifikasi_atasan;
    	}
    }

    public static function hitungNotifikasi($pegawai_id){
        Notifikasi::hitungNotifikasiSebagaiAtasan($pegawai_id);
        return Notifikasi::hitungNotifikasiSebagaiPegawai($pegawai_id);
    }

}
