<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarKinerjaBulanan extends Model
{
    //

    protected $table = 'komentar_kinerja_bulanan';

    public $timestamps = false;
}
