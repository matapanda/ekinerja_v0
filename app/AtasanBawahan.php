<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtasanBawahan extends Model
{
    //
    protected $table = 'atasan_bawahan';

    public $timestamps = false;

    protected $fillable = ['atasan_id', 'bawahan_id'];

    public function atasan(){
    	return $this->belongsTo('App\Pegawai', 'atasan_id');
    }

    public function bawahan(){
    	return $this->belongsTo('App\Pegawai', 'bawahan_id');
    }
}
