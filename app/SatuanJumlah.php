<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SatuanJumlah extends Model
{
    //
    protected $table = 'satuan_jumlah';

    public $timestamps = false;

    protected $fillable   = ['nama'];
}
