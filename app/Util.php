<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\IdentitasPegawai;

class Util 
{
    //
    
    const STATUS_BELUM_DIPERIKSA = 1;
    const STATUS_SETUJU = 2;
    const STATUS_TOLAK = 3;

    public static function get_all_bulan(){
        $bulan=array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        );
    	return $bulan;
    }

    public static function get_bulan($id){
        $bulan=array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        );
        return $bulan[$id];
    }

    public static function get_bulan_inverse($id){
        $bulan=array(
            'Januari' => 1,
            'Februari' => 2,
            'Maret' => 3,
            'April' => 4,
            'Mei' => 5,
            'Juni' => 6,
            'Juli' => 7,
            'Agustus' => 8,
            'September' => 9,
            'Oktober' => 10,
            'November' => 11,
            'Desember' => 12,
        );
        return $bulan[$id];
    }

    public static function get_bulan_inverse_with_check($id){
        $bulan=array(
            'Januari' => 1,
            'Februari' => 2,
            'Maret' => 3,
            'April' => 4,
            'Mei' => 5,
            'Juni' => 6,
            'Juli' => 7,
            'Agustus' => 8,
            'September' => 9,
            'Oktober' => 10,
            'November' => 11,
            'Desember' => 12,
        );
        if(array_key_exists($id, $bulan)){
            return $bulan[$id];
        }
        else{
            return 0;
        }
        
    }

    /*
    public static function getPegawaiId(){
        return Auth::user()->pegawai_id;
    }

    public static function getPegawai(){
        return Auth::user();
    }*/

    public static function getPegawaiId(){
        $username = Auth::user()->username;
        $pegawai = IdentitasPegawai::where('nip_baru', $username)->first();
        return $pegawai->id_pegawai;
    }

    public static function getPegawai(){
        $username = Auth::user()->username;
        $pegawai = IdentitasPegawai::where('nip_baru', $username)->first();
        return $pegawai;
    }

    public static function getPegawaiWithPosisi($pegawai_id){
        
        $pegawai = IdentitasPegawai::select('identitas_pegawai.*','posisi.keterangan as keterangan_posisi', 'unitk.keterangan as keterangan_unitk')
            ->leftJoin('jabatan_terakhir', 'jabatan_terakhir.id_pegawai', '=', 'identitas_pegawai.id_pegawai')
            ->leftJoin('posisi', 'posisi.id_posisi', '=', 'jabatan_terakhir.posisi')
            ->leftJoin('unitk', 'posisi.unitk', '=', 'unitk.id_unitk')
            ->where('identitas_pegawai.id_pegawai', $pegawai_id)->get();
            
        /*
        $pegawai = DB::connection("db_master")
            ->table("biodata as bio")
            ->select("bio.*", "ref_jbt.jab_struktural as keterangan_posisi", "rwyt.unit_kerja as keterangan_unitk", "rwyt.instansi as instansi")
            ->join("rwyt_jab_struktural as rwyt","rwyt.pegawai_id","=","bio.pegawai_id")
            ->join("ref.ref_jabatan_struktural as ref_jbt","rwyt.jab_struktural_id","=","ref_jbt.jab_struktural_id")
            ->where("bio.pegawai_id", $pegawai_id)->where("rwyt.aktif","Y")->first();*/
        return $pegawai;
    }

    public static function getStatus(){
        return array(
         "1" => "Belum diperiksa", 
         "2" => "Disetujui",
         "3" => "Ditolak");
    }
}
