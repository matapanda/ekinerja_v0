<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JabatanTerakhir extends Model
{
    //
    protected $table = 'jabatan_terakhir';

    public $timestamps = false;

    protected $connection = 'db_simpeg';
}
