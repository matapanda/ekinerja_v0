<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubUnitK extends Model
{
    //
     protected $table = 'sub_unitk';

     protected $connection = 'db_simpeg';

    public $timestamps = false;

    protected $primaryKey = 'id_sub_unitk';
}
