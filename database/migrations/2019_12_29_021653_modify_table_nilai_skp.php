<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTableNilaiSkp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nilai_skp_bulanan', function (Blueprint $table) {
            //
            $table->decimal('nilai_tugas_jabatan',2)->default(0)->change();
            $table->decimal('nilai_tugas_tambahan',2)->default(0)->change();
            $table->decimal('nilai_skp',2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nilai_skp_bulanan', function (Blueprint $table) {
            //
        });
    }
}
