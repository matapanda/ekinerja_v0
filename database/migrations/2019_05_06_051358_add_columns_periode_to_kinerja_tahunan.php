<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPeriodeToKinerjaTahunan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kinerja_tahunan', function (Blueprint $table) {
            $table->smallInteger("periode_awal")->default(1);
            $table->smallInteger("periode_akhir")->default(12);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kinerja_tahunan', function (Blueprint $table) {
            //
        });
    }
}
