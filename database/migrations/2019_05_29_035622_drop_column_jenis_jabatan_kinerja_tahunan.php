<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnJenisJabatanKinerjaTahunan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kinerja_tahunan', function (Blueprint $table) {
            //
            $table->dropColumn("jenis_jabatan");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kinerja_tahunan', function (Blueprint $table) {
            //
        });
    }
}
