<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyKinerjaBulanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kinerja_bulanan', function (Blueprint $table) {
            $table->string('nama');
            $table->integer('kinerja_tahunan_id');
            // Field untuk target
            $table->integer('target_kuantitas')->default(0);
            $table->tinyInteger('satuan_target_kuantitas')->nullable();
            $table->integer('target_waktu')->default(0);
            $table->tinyInteger('satuan_target_waktu')->nullable();
            $table->integer('target_kualitas')->default(0);
            $table->tinyInteger('status_target')->default(0);
            // Field untuk capaian
            $table->integer('capaian_kuantitas')->default(0);
            $table->integer('capaian_waktu')->default(0);
            $table->integer('capaian_kualitas')->default(0);
            $table->tinyInteger('status_capaian')->default(0);
            // Field skor akhir
            $table->decimal('skor')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kinerja_bulanan', function (Blueprint $table) {
            //
        });
    }
}
