<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKinerjaTahunan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kinerja_tahunan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->integer('pegawai_id');
            $table->integer('tahun');
            // Field untuk target
            $table->integer('target_kuantitas')->default(0);
            $table->tinyInteger('satuan_target_kuantitas')->nullable();
            $table->integer('target_waktu')->default(0);
            $table->tinyInteger('satuan_target_waktu')->nullable();
            $table->integer('target_kualitas')->default(0);
            $table->tinyInteger('status_target')->default(0);
            // Field untuk capaian
            $table->integer('capaian_kuantitas')->default(0);
            $table->integer('capaian_waktu')->default(0);
            $table->integer('capaian_kualitas')->default(0);
            $table->tinyInteger('status_capaian')->default(0);
            // Field skor akhir
            $table->decimal('skor')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kinerja_tahunan');
    }
}
