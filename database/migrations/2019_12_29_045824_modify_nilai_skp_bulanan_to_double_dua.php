<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyNilaiSkpBulananToDoubleDua extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nilai_skp_bulanan', function (Blueprint $table) {
            //
            $table->double('nilai_tugas_jabatan',2)->default(0.0);
            $table->double('nilai_tugas_tambahan',2)->default(0.0);
            $table->double('nilai_skp',2)->default(0.0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nilai_skp_bulanan', function (Blueprint $table) {
            //
        });
    }
}
