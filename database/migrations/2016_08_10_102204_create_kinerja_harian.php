<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKinerjaHarian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kinerja_harian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->integer('kinerja_bulanan_id');
            $table->integer('capaian_kuantitas')->default(0);
            $table->tinyInteger('satuan_target_kuantitas')->nullable();
            $table->timestamp('waktu_awal');
            $table->timestamp('waktu_akhir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kinerja_harian');
    }
}
