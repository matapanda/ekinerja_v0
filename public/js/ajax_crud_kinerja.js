var rules = {};

var messages = {};

function showDetail(detail_url, id, extra_param){
  $('#btn-submit').hide();
  var final_url = detail_url+'?show='+id;
  if(typeof extra_param != "undefined"){
      final_url = final_url+'&'+extra_param;    
  }
  $('#modal-title').html('Detail Kegiatan');
  $('#btn-submit').hide();
  $.ajax({
    url: detail_url+'?show='+id,
    success: function(data) {
      $("#dialog-form .modal-body").html(data);
      dialog.dialog('open');
    }
  });
}

function showForm(detail_url){
  $('#btn-submit').show();
  $.ajax({
    url: detail_url,
    success: function(data) {
      $("#dialog-form .modal-body").html(data);
      dialog.dialog('open');
    },
     error: function(xhr, status, error) {
        alert(xhr.responseText);
     }
  });
}

function generateUrl(action, detail_url, id, extra_param){
  var final_url = detail_url+action+id;
  if(typeof extra_param != "undefined"){
    if(action == ''){
      final_url = final_url+'?'+extra_param;
    }
    else{
      final_url = final_url+'&'+extra_param;
    }
    
  }
  return final_url;
}

function showInsertForm(detail_url, id, extra_param){
  $('#modal-title').html('Tambah Kegiatan');
  var final_url = generateUrl('/', detail_url, id, extra_param);
  showForm(final_url);
  $('#btn-submit').unbind('click');
  $('#btn-submit').click(function(){
    var submit_url = generateUrl('?insert=', detail_url, 1, extra_param);
    submitForm(submit_url);
  });
}

function showUpdateForm(detail_url, id, extra_param){    
  $('#modal-title').html('Ubah Kegiatan');
  var final_url = generateUrl('?update=', detail_url, id, extra_param);
  showForm(final_url); 
  $('#btn-submit').unbind('click');
  $('#btn-submit').click(function(){
    var submit_url = generateUrl('?update=', detail_url, id, extra_param);
    submitForm(submit_url);
  });  
}

function showDeleteForm(detail_url, id, extra_param){
  $('#modal-title').html('Hapus Kegiatan');
  var final_url = generateUrl('?delete=', detail_url, id, extra_param);
  showForm(final_url); 
  $('#btn-submit').unbind('click');
  $('#btn-submit').click(function(){
    var submit_url = generateUrl('?do_delete=', detail_url, id, extra_param);
    doSubmitForm(submit_url);
  });
}

function doSubmitForm(detail_url){
  var submit_data = $(".form-horizontal").serializeArray();
  //submit_data['save'] = 1;
  submit_data.push({name:'save', value:1});
  //console(submit_data);
  $.ajax({
         type: "POST",
         url: detail_url,
         data: submit_data, // serializes the form's elements.
         success: function(data)
         {
             alert('Sukses');
             closeDialog();
             
             //tahun = $('#tahun').val();               
             //showTabelTahunanBulanan(tahun);
             refreshDropdownTahun();
         },
         error: function(xhr, status, error) {
            alert(xhr.responseText);
         }
       });
}

function submitForm(detail_url){
  //$('#form-submit').attr('action', detail_url);
  $('#form-submit').validate({
    rules : rules,
    messages : messages,
    submitHandler : function() {
      doSubmitForm(detail_url);
    }
  });       
  $('#form-submit').submit();
}

function closeDialog(){
  dialog.dialog('close');
}