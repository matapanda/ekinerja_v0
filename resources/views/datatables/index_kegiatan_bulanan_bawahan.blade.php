@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li class="active">{{ $page_title }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
Daftar
@endsection

@section('extra_css')
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('extra_js')

<script type="text/javascript">
  
  $(document).ready(function () {
    dialog = $('#dialog-form').dialog({
      autoOpen: false,
      modal: true
    });    
  });

  function showDetail(detail_url, id){
    startLoading();
    $('#btn-submit').hide();
    $.ajax({
      url: detail_url+id,
      success: function(data) {
        stopLoading();
        $("#dialog-form .modal-body").html(data);
        dialog.dialog('open');
      },
      error: function(xhr, status, error) {
        stopLoading();
        console(xhr.responseText);
        alert("Terjadi Kesalahan pada Sistem");
      }
    });
  }

  function closeDialog(){
    dialog.dialog('close');
  }
</script>

<script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
startLoading();
var oTable = $('#tabel_index').DataTable({
      processing: true,
      serverSide: true,
      //deferLoading : 1,
      "responsive": true,
      drawCallback: function( settings ) {
        stopLoading();
      },
      ajax: {
        url : '{{ url("kegiatan_bulanan_bawahan/data") }}',
        data: function (d) {
          d.pegawai = $('#pegawai').val();
          d.bulan = $('#bulan').val();
          d.tahun = $('#tahun').val();
        }
      },
      sDom: 'p',
      aoColumns: [
                        { mData: 'nama', sWidth: "20%" },    
                        { mData: 'jenis', sWidth: "7%" },                    
                        { mData: 'bulan', sWidth: "7%" },
                        { mData: 'capaian_kuantitas' , sWidth: "5%" },
                        { mData: 'capaian_kualitas', sWidth: "5%" },
                        { mData: 'status', sWidth: "5%" },
                        { mData: 'action', sWidth: "15%" }
                ],
      columnDefs: [
        { className: 'text-center', targets: [5,6] },
      ]
  });

$(function() {
  //oTable.draw();
});

function refreshTable(){
  startLoading();
  oTable.draw();
}

function showForm(detail_url){
  startLoading();
  $('#btn-submit').show();
  $.ajax({
    url: detail_url,
    success: function(data) {
      $("#dialog-form .modal-body").html(data);
      stopLoading();
      dialog.dialog('open');
    },
     error: function(xhr, status, error) {
        console(xhr.responseText);
        stopLoading();
        alert("Terjadi Kesalahan pada Sistem");
     }
  });
}

function showFormChangeStatus(base_url, id, status){
  url = base_url +'/?update='+id; 
  showForm(url);
  $('#btn-submit').unbind('click');
  $('#btn-submit').click(function(){
    komentar = $('#komentar').val();
    capaian_kualitas = 0;
    capaian_kuantitas = 0;
    if(status == 2){
      capaian_kualitas = $('#capaian_kualitas').val();
      capaian_kuantitas = $('#capaian_kuantitas').val();
    }
    changeStatus(base_url, id, status, komentar, capaian_kualitas, capaian_kuantitas);
    closeDialog();
  });
}

function changeStatus(base_url, id, status, komentar, capaian_kualitas, capaian_kuantitas){
  startLoading();
  $.ajax({
    type: 'POST',
    url: base_url+'?update='+id,
    headers: {
        'Content-Type' : 'application/x-www-form-urlencoded'
    },
    data: { 
        '_method': 'PATCH', 
        '_token': '{!! csrf_token() !!}',
        'save' : 1,
        'status' : status,
        'komentar' : komentar,
        'capaian_kualitas' : capaian_kualitas,
        'capaian_kuantitas' : capaian_kuantitas
    },
    success: function(data) {
      stopLoading();
      alert('Status berhasil diubah');
      refreshTable();
    },
     error: function(xhr, status, error) {
      stopLoading();
      console(xhr.responseText);
      alert("Terjadi Kesalahan pada Sistem");
     }
  });
}

function accept(accept_url, id){
  showFormChangeStatus(accept_url, id, 2);
}

function reject(accept_url, id){
  showFormChangeStatus(accept_url, id, 3);
}

</script>
@endsection



@section('content')
<div class="modal" id="dialog-form" style="max-height: 5px">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeDialog()"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Detail Kegiatan</h4>
      </div>
      <div class="modal-body col-lg-12" style="max-height: calc(100vh - 210px);overflow-y: auto;">
        <p>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeDialog()">Keluar</button>
        <button id='btn-submit' style="display:none" type="button" class="btn btn-primary">Kirim</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<div class="row">
            <div class="col-xs-12">
              <div class="box">                
                <div class="box-body">
                  @if(Session::has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>                    
                    {!! Session::get('message') !!}
                  </div>
                  @endif
                  
                  
                  <label>Pegawai</label>
                  {!! Form::select('pegawai', $list_pegawai, $selected_pegawai, $options = array('id' => 'pegawai', 'class' => 'form-control', 'style'=>'width:15%; display: inline; margin-left: 10px; margin-left: 10px')) !!}
                  <label>Tahun</label>
                  {!! Form::select('tahun', $list_tahun, $selected_tahun, $options = array('id' => 'tahun', 'class' => 'form-control', 'style'=>'width:7%; display: inline; margin-left: 10px; margin-left: 10px')) !!}                  
                  <label>Bulan</label>
                  {!! Form::select('bulan', $list_bulan, $selected_bulan, $options = array('id' => 'bulan', 'class' => 'form-control', 'style'=>'width:7%; display: inline; margin-left: 10px; margin-left: 10px')) !!}                  
                  <a class="btn btn-primary" href="#" onclick="refreshTable()" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">Tampilkan</a>              


                  <table id="tabel_index" class="table table-condensed">
                  <thead>
                  <tr>
                      <th style="text-align:left;">Nama</th>
                      <th style="text-align:left;">Jenis Kegiatan</th>
                      <th style="text-align:left;">Bulan</th>
                      <th style="text-align:left;">Capaian Kuantitas (%)</th>
                      <th style="text-align:left;">Capaikan Kualitas (%)</th>
                      <th style="text-align:center;">Status</th>
                      <th style="text-align:center;">Action</th>
                  </tr>
                  </thead>
              </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection