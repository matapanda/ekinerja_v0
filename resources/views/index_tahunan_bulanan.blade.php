@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li class="active">{{ $page_title }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
Daftar
@endsection

@section('extra_css')
@endsection

@section('extra_js')
<script type="text/javascript">
  function showTabelTahunanBulanan(tahun, posisi){
    $.ajax({
      url: '/kinerja_tahunan_bulanan/tahunanbulanan',
      data: { 
          'tahun': tahun,
          'posisi': posisi
      },
      success: function(data) {
        $("#tabel-kinerja").html(data);
      }
    });
  } 

  function filterTabelTahunanBulanan(){ 
    tahun = $('#tahun').val();
    posisi = $('#posisi').val();
    showTabelTahunanBulanan(tahun, posisi);
  }

  function refreshDropdownTahun(){
    $.ajax({
      url: '/kinerja_tahunan_bulanan/tahun',      
      success: function(data) {
        var $el = $("#tahun");
        $el.empty();
        $.each(data, function(value, key) {            
            $el.append($("<option></option>")
                    .attr("value", value).text(key));
        });
        // Refresh tabel setelah refresh tahun
        filterTabelTahunanBulanan();
      }
    });
  }

  $(document).ready(function () {
    dialog = $('#dialog-form').dialog({
      autoOpen: false,
      modal: true,
      width: 100,
      maxWidth: 100
    });
    refreshDropdownTahun();    
  });

  
</script>
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/ajax_crud_kinerja.js') }}"></script>
@endsection

@section('content')
<div class="modal" id="dialog-form" style="max-height: 5px">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeDialog()"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="modal-title">Detail Kegiatan</h4>
      </div>
      <div class="modal-body col-lg-12" style="max-height: calc(100vh - 210px);overflow-y: auto;top:0;">
        <p>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeDialog()">Keluar</button>
        <button id='btn-submit' style="display:none" type="button" class="btn btn-primary">Kirim</button>
      </div>
    </div>
  </div>
</div>
<div class="row">
            <div class="col-xs-12">
              <div class="box">                
                <div class="box-body">
                @if(Session::has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>                    
                    {!! Session::get('message') !!}
                  </div>
                  @endif
                  <div class="pull-left" style="width:50%">
                  {!! Form::select('tahun', array(), null, $options = array('id' => 'tahun', 'class' => 'form-control', 'style'=>'width:20%; display: inline')) !!}

                  {!! Form::select('posisi', $list_posisi, null, $options = array('id' => 'posisi', 'class' => 'form-control', 'style'=>'width:30%; display: inline')) !!}

                   <a class="btn btn-success" href="#" onclick="filterTabelTahunanBulanan()" style="width:10%; display: inline; margin-left: 10px; margin-left: 10px">Tampilkan</a>

                   
                   <a class="btn btn-warning" href="/kinerja_tahunan_bulanan/exportskp" style="width:20%; display: inline; margin-left: 10px; margin-left: 10px">Generate</a>
                    

                   </div>

                   <div class="pull-right" style="width:50%">
                   
                   <div class="form-group pull-right" style="padding-left: 10px">
                    <a class="btn btn-default" href="{{ asset('TemplateImportSKP.xls') }}" alt="Import">Template SKP</a>
                    </div>

                   {!! Form::open(array('url' => '/kinerja_tahunan_bulanan/importskp', 'files' => true, 'class'=>'form-inline')) !!}
                    <div class="form-group pull-right">
                     {!! Form::submit('Import', array('class'=>'btn btn-primary')) !!}
                     </div>

                     <div class="form-group pull-right">
                     {!! Form::file('skp') !!}
                     </div>
                     

                    {!! Form::close() !!}
                   <!--
                   <a class="btn btn-success pull-right" href="/kinerja_tahunan_bulanan/formbulanan" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px"><i class="fa fa-plus"></i> Kegiatan Bulanan</a> -->
                    <!--<a class="btn btn-primary pull-right" href="/kinerja_tahunan_bulanan/form" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">-->

                    
                                      
                    
                    </div>
                      <div class="pull-left" style="width:100%; padding-top: 10px;">
                      <a class="btn btn-primary" href="#" onclick="showInsertForm('/kinerja_tahunan_bulanan/form','','')" style="width:20%; display: inline; margin-left: 10px; margin-left: 0px;">
                      <i class="fa fa-plus"></i> Kegiatan Tahunan</a>
                      </div>



                     <div id="tabel-kinerja" style="padding-top: 100px">                     
                     </div>
                   </div>                    
                   </div>
                   
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection