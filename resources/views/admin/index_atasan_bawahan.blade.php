@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li class="active">{{ $page_title }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
Daftar
@endsection

@section('extra_css')
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('extra_js')
<script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
$(function() {
  $('#tabel_index').DataTable({
      processing: true
  });
});
  
</script>
@endsection

@section('content')

<div class="row">
            <div class="col-xs-12">
              <div class="box">                
                <div class="box-body">
                  @if(Session::has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>                    
                    {!! Session::get('message') !!}
                  </div>
                  @endif
                  <table id="tabel_index" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="tabel_kinerja_info">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-sort="ascending" aria-label="NIP Pegawai">NIP Pegawai</th>
                      <th class="sorting_asc" tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nama Kegiatan">Nama Pegawai</th>
                      <th class="sorting" tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-label="Pegawai">NIP Atasan</th>
                      <th class="sorting" tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-label="Pegawai">Nama Atasan</th>
                      <th aria-label="Action">Action</th></tr>
                  </thead>
                  <tbody>
                    @foreach($list_atasan as $data)
                      <tr>
                        <td>{{$data->nip_bawahan}}</td>
                        <td>{{$data->nama_bawahan}}</td>
                        <td>{{$data->nip_atasan}}</td>
                        <td>{{$data->nama_atasan}}</td>
                        <td><a class="" title="Edit" href="/atasanbawahan/form?pegawai_id={{$data->id_bawahan}}"><span class="glyphicon glyphicon-edit"> </span></a></td>
                      </tr>
                    @endforeach
                  </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection