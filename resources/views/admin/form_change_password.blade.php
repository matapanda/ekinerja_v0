@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li><a href={{ $base_url }}>{{ $page_title }}</a></li>
            <li class="active">{{ $page_activity }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
{{ $page_activity }}
@endsection


@section('content')

<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                @if(Session::has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>                    
                    {!! Session::get('message') !!}
                  </div>
                  @endif
                <form method="POST" action="/user/changepassword/{{$user->id}}" accept-charset="UTF-8" class="form-horizontal" role="form">
                    <div class="btn-toolbar" role="toolbar">
                        <div class="pull-right">
                            <a href="/user" class="btn btn-default">Batal</a>
                        </div>
                    </div>
                <br>
                    <div class="form-group clearfix" id="fg_username">
                        <label for="username" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-10" id="div_username">
                            <div class="help-block">{{ $user->username }}&nbsp;</div>
                        </div>
                    </div>
                    <div class="form-group clearfix" id="fg_password">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10" id="div_password">
                            <input class="form-control form-control" type="password" id="password" name="password" value="">
                        </div>
                    </div>

        
            <div class="btn-toolbar" role="toolbar">

                
                <div class="pull-right">
             <input class="btn btn-primary" type="submit" value="Simpan">
                    </div>
        
            </div>
    <br>
        <input name="save" type="hidden" value="1"></form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection