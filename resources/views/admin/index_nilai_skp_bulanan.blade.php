@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li class="active">{{ $page_title }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
Daftar
@endsection

@section('extra_css')
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('extra_js')

<script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
startLoading();
var oTable = $('#tabel_index').DataTable({
      processing: true,
      serverSide: true,
      //deferLoading : 1,
      "responsive": true,
      drawCallback: function( settings ) {
        stopLoading();
      },
      ajax: {
        url : '{{ url("admin_nilai_skp_bulanan/data") }}',
        data: function (d) {
          d.bulan = $('#bulan').val();
          d.tahun = $('#tahun').val();
        }
      },
      sDom: 't',
      aoColumns: [
                        { mData: 'nama', sWidth: "10%" },    
                        { mData: 'nilai_tugas_jabatan', sWidth: "10%" },                    
                        { mData: 'nilai_tugas_tambahan', sWidth: "10%" },
                        { mData: 'nilai_skp' , sWidth: "10%" },
                        { mData: 'action', sWidth: "15%" }
                ],
      columnDefs: [
        { className: 'text-center', targets: [1,2,3,4] },
      ]
  });

$(function() {
  oTable.draw();
});

function refreshTable(){
  startLoading();
  oTable.draw();
}

function hitungUlang(pegawai_id){
  startLoading();
  bulan = $('#bulan').val();
  tahun = $('#tahun').val();
  $.ajax({
    type: 'POST',
    url: '{{ url("admin_nilai_skp_bulanan/hitung") }}'+'/'+tahun+'/'+bulan+'/'+pegawai_id,
    headers: {
        'Content-Type' : 'application/x-www-form-urlencoded'
    },
    data: { 
        
    },
    success: function(data) {
      stopLoading();
      alert('Nilai SKP berhasil dihitung');
      refreshTable();
    },
     error: function(xhr, status, error) {
      stopLoading();
      console(xhr.responseText);
      alert("Terjadi Kesalahan pada Sistem");
     }
  });
}

function hitungUlangSemua(){
  startLoading();
  bulan = $('#bulan').val();
  tahun = $('#tahun').val();
  $.ajax({
    type: 'POST',
    url: '{{ url("admin_nilai_skp_bulanan/hitungsemua") }}'+'/'+tahun+'/'+bulan,
    headers: {
        'Content-Type' : 'application/x-www-form-urlencoded'
    },
    data: { 
        
    },
    success: function(data) {
      stopLoading();
      alert('Nilai SKP berhasil dihitung');
      refreshTable();
    },
     error: function(xhr, status, error) {
      stopLoading();
      console(xhr.responseText);
      alert("Terjadi Kesalahan pada Sistem");
     }
  });
}


</script>
@endsection



@section('content')

<div class="row">
            <div class="col-xs-12">
              <div class="box">                
                <div class="box-body">
                  @if(Session::has('message'))
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>                    
                    {!! Session::get('message') !!}
                  </div>
                  @endif
                  
                  
                  <label>Tahun</label>
                  <input type="text" value="{{$selected_tahun}}" name="tahun" id="tahun" />
                  <label>Bulan</label>
                  {!! Form::select('bulan', $list_bulan, $selected_bulan, $options = array('id' => 'bulan', 'class' => 'form-control', 'style'=>'width:7%; display: inline; margin-left: 10px; margin-left: 10px')) !!}                  
          
                  <a class="btn btn-primary" href="#" onclick="refreshTable()" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">Tampilkan</a>              
                  <a class="btn btn-danger" href="#" onclick="hitungUlangSemua()" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">Hitung</a>              

                  <table id="tabel_index" class="table table-condensed">
                  <thead>
                  <tr>
                      <th style="text-align:left;">Nama</th>
                      <th style="text-align:left;">Nilai Tugas Jabatan</th>
                      <th style="text-align:left;">Nilai Tugas Tambahan</th>
                      <th style="text-align:left;">Nilai SKP</th>
                      <th style="text-align:center;">Action</th>
                  </tr>
                  </thead>
              </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection