@if($edit->status == 'show')
<div class="col-sm-12">
<label class="col-sm-4 control-label required">
	Tahun
</label>                   			
<div class="col-sm-8">
	<div class="help-block">
	{!! $edit->field('tahun') !!}
</div>
</div>
</div>

<div class="col-sm-12">
<label class="col-sm-4">
	Periode
</label>                   			
<div class="col-sm-1">
	{!! $edit->field('periode_awal') !!}
</div>
<div class="col-sm-1">
	<div style="padding-top:5px;text-align:center;"> sampai </div>
</div>
<div class="col-sm-5">
	{!! $edit->field('periode_akhir') !!}
</div>
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('nama')->req !!}">
	{!! $edit->field('nama')->label !!}
</label>                   			
<div class="col-sm-8">
{!! $edit->field('nama') !!}
</div>
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('deskripsi')->req !!}">
{!! $edit->field('deskripsi')->label !!}
</label>                        
<div class="col-sm-8">
@if($edit->status == 'show')                         
{!! $edit->field('deskripsi')->value !!}
@else
{!! $edit->field('deskripsi') !!}
@endif
</div>
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('target_kuantitas')->req !!}">
	{!! $edit->field('target_kuantitas')->label !!}
</label>
<div class="col-sm-1">
{!! $edit->field('target_kuantitas') !!}
</div>                   
<div class="col-sm-7">
{!! $edit->field('satuan_target_kuantitas') !!}
</div>                        
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('target_waktu')->req !!}">
	{!! $edit->field('target_waktu')->label !!}
</label>
<div class="col-sm-1">
{!! $edit->field('target_waktu') !!}
</div>                   
<div class="col-sm-7">
<!--
{!! $edit->field('satuan_target_waktu') !!}
-->
<div class="help-block">bulan&nbsp;</div>
</div>                        
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('target_kualitas')->req !!}">
	{!! $edit->field('target_kualitas')->label !!}
</label>
<div class="col-sm-1">
{!! $edit->field('target_kualitas') !!}
</div>
<div class="col-sm-3" style="padding-top:5px;">
    %
</div>
</div>

@if($edit->status == 'show')
<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('capaian_kuantitas')->req !!}">
	{!! $edit->field('capaian_kuantitas')->label !!}
</label>
<div class="col-sm-1">
{!! $edit->field('capaian_kuantitas') !!}
</div>		                    
<div class="col-sm-7">
{!! $edit->field('satuan_target_kuantitas') !!}
</div>
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('capaian_waktu')->req !!}">
	{!! $edit->field('capaian_waktu')->label !!}
</label>
<div class="col-sm-1">
{!! $edit->field('capaian_waktu') !!}
</div>		                    
<div class="col-sm-7">
<!--{!! $edit->field('satuan_target_waktu') !!}-->
<div class="help-block">bulan&nbsp;</div>
</div>
</div>

<div class="col-sm-12">
<label class="col-sm-4 control-label{!! $edit->field('capaian_kualitas')->req !!}">
	{!! $edit->field('capaian_kualitas')->label !!}
</label>
<div class="col-sm-1">
{!! $edit->field('capaian_kualitas') !!}
</div>
<div class="col-sm-3" style="padding-top:5px;">
    %
</div>
</div>
@endif

@else
{!! Rapyd::styles() !!}
{!! Rapyd::scripts() !!}
<script src="{{ asset('admin-lte/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	CKEDITOR.config.startupFocus = false;
	//CKEDITOR.replace( 'deskripsi');
	var rules = {
		nama : 'required',
		target_kuantitas: 'required',
		target_kuantitas: 'number',
		target_kualitas : 'required',
		target_kualitas : 'number',
		target_waktu : 'required',
		target_waktu : 'number'
	};

	var messages = {
		nama : 'Nama kegiatan wajib diisi',
		target_kuantitas: 'Target Kuantitas wajib diisi berupa angka',
		target_kualitas : 'Target Kualitas wajib diisi berupa angka',
		target_waktu : 'Target Waktu wajib diisi berupa angka'
	};
</script>
<form id="form-submit" class="form-horizontal" method="POST">
	{!! $edit->open !!}

	{!! $edit->message !!}
	@if(!$edit->message)
		<div class="form-group clearfix">
			<label class="col-sm-2 control-label required">
				Tahun
			</label>                   			
			<div class="col-sm-10">
				<div class="help-block">
				@if ($edit->status == 'create')
			    	{!! $tahun !!}
			    @else
			    	{!! $edit->field('tahun') !!}
			    @endif
			    </div>
		    </div>
	    </div>		                    

	    @if(isset($nama_pegawai))
	    <div class="form-group clearfix {!! $edit->field('nama')->has_error !!}">
			<label class="col-sm-2 control-label{!! $edit->field('nama')->req !!}">
				Nama Pegawai
			</label>                   			
			<div class="col-sm-10">
	        {!! $nama_pegawai !!}
	    </div>
	    </div>
	    @endif

			<div class="form-group clearfix {!! $edit->field('nama')->has_error !!}">
			<label class="col-sm-2 control-label{!! $edit->field('nama')->req !!}">
				{!! $edit->field('nama')->label !!}
			</label>                   			
			<div class="col-sm-10">
	        {!! $edit->field('nama') !!}
	        <label>{!! $edit->field('nama')->message !!}</label>
	    </div>
	    </div>

	    <div class="form-group clearfix {!! $edit->field('deskripsi')->has_error !!}">
	    <label class="col-sm-2 control-label{!! $edit->field('deskripsi')->req !!}">
	      {!! $edit->field('deskripsi')->label !!}
	    </label>                        
	    <div class="col-sm-10">              	    @if($edit->status == 'show')
	        {!! $edit->field('deskripsi')->value !!}
	        @else
	        {!! $edit->field('deskripsi') !!}
	        @endif
	        <label>{!! $edit->field('deskripsi')->message !!}</label>
	    </div>
	    </div>

	    <div class="form-group clearfix {!! $edit->field('target_kuantitas')->has_error !!}">
	    <label class="col-sm-2 control-label{!! $edit->field('target_kuantitas')->req !!}">
				{!! $edit->field('target_kuantitas')->label !!}
			</label>
			<div class="col-sm-2">
	        {!! $edit->field('target_kuantitas') !!}
	    <label>{!! $edit->field('target_kuantitas')->message !!}</label>
	    </div>                   
	    <div class="col-sm-5">
	        {!! $edit->field('satuan_target_kuantitas') !!}
	    </div>                        
	    </div>

	    <div class="form-group clearfix {!! $edit->field('target_waktu')->has_error !!}">
	    <label class="col-sm-2 control-label{!! $edit->field('target_waktu')->req !!}">
				{!! $edit->field('target_waktu')->label !!}
			</label>
			<div class="col-sm-2">
	        {!! $edit->field('target_waktu') !!}
	    <label>{!! $edit->field('target_waktu')->message !!}</label>
	    </div>                   
	    <div class="col-sm-5">
	    bulan
	    	<!--
	        {!! $edit->field('satuan_target_waktu') !!}
	        -->
	    </div>                        
	    </div>

	    <div class="form-group clearfix {!! $edit->field('target_kualitas')->has_error !!}">
	    <label class="col-sm-2 control-label{!! $edit->field('target_kualitas')->req !!}">
				{!! $edit->field('target_kualitas')->label !!}
			</label>
	    <div class="col-sm-5">
	        {!! $edit->field('target_kualitas') !!}
	        <label>{!! $edit->field('target_kualitas')->message !!}</label>
	    </div>
	    </div>
	@endif
</form>      
@endif