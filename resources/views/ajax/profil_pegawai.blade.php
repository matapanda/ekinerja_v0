<div class="box-body box-profile">   
<div class="col-md-3">    
@if( $pegawai->foto && file_exists( public_path().'/upload/'.$pegawai->foto) )
<img src="{{ asset('upload/'.$pegawai->foto) }}" class="profile-user-img img-responsive img-circle" alt="User Image">
@else
<img src="{{ asset('user.jpg') }}" class="profile-user-img img-responsive img-circle" alt="User Image">
@endif
<h3 class="profile-username text-center">{{ $pegawai->nama }}</h3>
<p class="text-muted text-center">{{ $pegawai->nip_baru }}</p>
</div>

<div class="col-md-9">
<strong><i class="fa fa-bookmark margin-r-5"></i> Jabatan</strong>
<p class="text-muted">{{ $pegawai->keterangan_posisi }}
 {{ $pegawai->keterangan_unitk }}</p>
                 
<strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>
<p class="text-muted">{{ $pegawai->alamat }}</p>  

</div>               
</div><!-- /.box-body -->