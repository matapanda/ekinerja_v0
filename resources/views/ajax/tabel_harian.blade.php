<table id="tabel_kinerja" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="tabel_kinerja_info">
<thead>
  <tr role="row">
    <th class="sorting_asc" tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nama Kegiatan">Nama Kegiatan</th>
    <th class="sorting" tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-label="Pegawai">Pegawai</th>
    <th class="sorting" tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-label="Tanggal">Tanggal</th>
    <th tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-label="Waktu">Waktu</th>
    <th tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-label="Capaian Kuantitas">Capaian Kuantitas</th>
    <th tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-label="Kegiatan Bulanan">Kegiatan Bulanan</th>
    <th tabindex="0" aria-controls="tabel_kinerja" rowspan="1" colspan="1" aria-label="Status">Status</th>
    <th aria-label="Action">Action</th></tr>
</thead>
<tbody>
  <?php $odd=true; ?>
  @foreach($kinerja_harian as $tanggal => $list_kinerja)      
      @foreach($list_kinerja as $kinerja)
        <tr role="row" class={{ $odd === true ? "odd" : "even" }}>
          <td><a href="#" onclick=showDetail('/kinerja_harian_bawahan/form?show=',{{$kinerja->id}})>{{ $kinerja->nama }}</a></td>
          <td>{{$kinerja->pegawai->nama}}</td>
          <td>{{ $tanggal }}</td>
          <td>{{$kinerja->waktu_awal}} - {{$kinerja->waktu_akhir}}</td>

          <td>{{$kinerja->capaian_kuantitas}} {{$kinerja->nama_satuan_jumlah }}</td>
          <td>{{$kinerja->nama_kegiatan_bulanan }}</td>
          <td>
            @if($kinerja->status == App\Util::STATUS_SETUJU)
              <i class="fa fa-check" id="harian_{{$kinerja->id}}"></i>
            @elseif($kinerja->status == App\Util::STATUS_TOLAK)
              <i class="fa fa-times" id="harian_{{$kinerja->id}}"></i>
            @else
              <i class="fa fa-question" id="harian_{{$kinerja->id}}"></i>
            @endif
          </td>

          <td>
			<a class="btn btn-primary btn-xs" href="#" onclick=showDetail('/kinerja_harian_bawahan/form?show=',{{$kinerja->id}})>Detail</a>
			<a class="btn btn-success btn-xs" href="#" onclick=accept('/kinerja_harian_bawahan/accept',{{$kinerja->id}},'harian_')>Setujui</a>
            <a class="btn btn-danger btn-xs" href="#" onclick=reject('/kinerja_harian_bawahan/reject',{{$kinerja->id}},'harian_')>Tolak</a>
          </td>
        </tr>
        <?php $odd=!$odd; ?>
      @endforeach
  @endforeach
</tbody>
</table>