
	<div class="col-sm-12">
	<label class="col-sm-2 control-label required">
		Tahun
	</label>                   			
	<div class="col-sm-10">
		<div class="help-block">
		@if ($edit->status == 'create')
				{!! $tahun !!}
			@else
				{!! $edit->field('tahun') !!}
			@endif
			</div>
	</div>
	</div>		                    

	<div class="col-sm-12">
	<label class="col-sm-2 control-label{!! $edit->field('nama')->req !!}">
		{!! $edit->field('nama')->label !!}
	</label>                   			
	<div class="col-sm-10">
			{!! $edit->field('nama') !!}
			<label>{!! $edit->field('nama')->message !!}</label>
	</div>
	</div>

	<div class="col-sm-12">
		<label class="col-sm-2 control-label{!! $edit->field('deskripsi')->req !!}">
			{!! $edit->field('deskripsi')->label !!}
		</label>                        
		<div class="col-sm-10">              	    @if($edit->status == 'show')
				{!! $edit->field('deskripsi')->value !!}
				@else
				{!! $edit->field('deskripsi') !!}
				@endif
				<label>{!! $edit->field('deskripsi')->message !!}</label>
		</div>
		</div>

<div class="col-sm-12">
	<label class="col-sm-2 control-label{!! $edit->field('target_waktu')->req !!}">
		{!! $edit->field('target_waktu')->label !!}
	</label>
	<div class="col-sm-2">
			{!! $edit->field('target_waktu') !!}
	</div>                   
		<div class="col-sm-5">
		bulan
		</div>                        
</div>

