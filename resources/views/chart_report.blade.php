@extends('base')

@section('page_title')
Laporan
@endsection

@section('extra_css')
<link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
@endsection

@section('content')
<!-- Small boxes (Stat box) -->
          <!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <!-- solid sales graph -->
          
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-md-12">
                  <div class="rpd-dataform inline">
                    <form class="form-inline" role="form">
                      <div class="form-group" id="fg_nama">
                        <label for="nama" class="sr-only">Tahun</label>
                          <span id="div_tahun">
                            <input class="form-control form-control" style="width:100px;" placeholder="Tahun" type="text" id="tahun" name="tahun" value="{{date("Y")}}">
                          </span>
                      </div>
                      <div class="form-group" id="fg_status" >
                        <label for="status" class="sr-only">Pegawai</label>
                          <span id="div_status">
                            <select class="form-control form-control" type="select" id="pegawai" name="pegawai" style="min-width:250px;"><option value="{{$pegawai_global->id_pegawai}}" selected="selected">{{$pegawai_global->nama}}</option></select>
                          </span>
                      </div>
                      <input class="btn btn-primary" type="submit" value="Tampilkan" id="tampilkan">
                     </form> 
                    </div>
                    <br>
                    <ul class="nav nav-pills type">
                      <li class="active"><a href="#" type='1' >Laporan Jumlah Kegiatan Harian</a></li>
                      <li><a href="#" type='2'>Laporan Rata-rata Kualitas Kegiatan</a></li>
                      <li><a href="#" type='3'>Laporan Rata-rata Capaian Kuantitas Kegiatan </a></li>
                      <li><a href="#" type='4'>Laporan Jumlah jam kerja</a></li>
                    </ul>
                  </div>
                </div>
                <div class="row">

                  <div class="col-lg-12" > 
                    <br>
                    <br>
                    <h3 class="box-title" id="title" style="text-align:center; width: 100%; margin: 0 auto;" >Laporan Jumlah Kegiatan Harian per Bulan</h3>
                    <br>
                    <br>
                    
                  </div>
                </div>

                <div class="row">

                  <div class="col-md-12"> 
                    <div id="stats-container" style="height: 250px; text-align:center; width: 90%; margin: 0 auto;"></div>
                  </div>
                </div>
              
          </div><!-- /.box -->
          
            </section>
          
          </div>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>

<script>
$(function() {
  // Create a function that will handle AJAX requests
  function requestData(tipe, tahun, id_peg, chart){

    $.ajax({
      type: "GET",
      url: "{{url('getReport/')}}", // This is the URL to the API
      data: { tipe: tipe, tahun:tahun, id_peg:id_peg }
    })
    .done(function( data ) {
      // When the response to the AJAX request comes back render the chart with new data
      chart.setData(JSON.parse(data));
      chart.options.yLabelFormat= function (y) { 
            if(tipe==1) return (Math.round( y * 10 ) / 10).toString() + ' kali';
            else if(tipe==2) return (Math.round( y * 10 ) / 10).toString() + ' %';
            else if(tipe==3) return (Math.round( y * 10 ) / 10).toString() + ' %';
            else if(tipe==4) return (Math.round( y * 10 ) / 10).toString() + ' Jam';
          };
      chart.redraw();
    })
    .fail(function() {
      // If there is no communication between the server, show an error
      alert( "error occured" );
    });
  }

  var chart = Morris.Bar({
    // ID of the element in which to draw the chart.
    element: 'stats-container',
    // Set initial data (ideally you would provide an array of default data)
    data: [0,0],
    xkey: 'nama_bulan',
    ykeys: ['nilai'],
    labels: ['Nilai'],
    hideHover: 'auto',
    xLabelAngle:'45'
  });
  
  // Request initial data for the past 7 days:
  requestData(1, 0, 0, chart);

  $('ul.type a').click(function(e){
    e.preventDefault();
    // Get the number of days from the data attribute
    var el = $(this);
    tipe = el.attr('type');
    if(tipe==1) $("#title").html("Laporan Jumlah Kegiatan Harian");
    else if (tipe==2) $("#title").html("Laporan Rata-rata Kualitas Kegiatan");
    else if (tipe==3) $("#title").html("Laporan Persentase Capaian Kuantitas Kegiatan");
    else if (tipe==4) $("#title").html("Laporan Jumlah jam kerja");
    var tahun = $('#tahun').val();
    var id_peg = $('#pegawai').val();
    // Request the data and render the chart using our handy function
    requestData(tipe, tahun, id_peg, chart);
    // Make things pretty to show which button/tab the user clicked
    el.parent().addClass('active');
    el.parent().siblings().removeClass('active');
    
  });
  $('#tampilkan').click(function(e){
    e.preventDefault();
    // Get the number of days from the data attribute
    
    tipe = 1;
    $("#title").html("Laporan Jumlah Kegiatan Harian");
    var tahun = $('#tahun').val();
    var id_peg = $('#pegawai').val();
    // Request the data and render the chart using our handy function
    requestData(tipe, tahun, id_peg, chart);
    // Make things pretty to show which button/tab the user clicked
    var el = $('ul.type li').first();
    el.addClass('active');
    el.siblings().removeClass('active');
    
  });

});

</script>
@endsection