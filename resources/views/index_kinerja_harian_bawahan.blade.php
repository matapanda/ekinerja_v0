@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li class="active">{{ $page_title }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
Daftar
@endsection

@section('extra_css')
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datetimepicker/datetimepicker3.css') }}">
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection

@section('extra_js')
<script src="{{ asset('admin-lte/plugins/datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/datetimepicker/locales/bootstrap-datetimepicker.id.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/kinerja_harian_bawahan.js') }}"></script>
<script type="text/javascript">

  

  $(document).ready(function () {    

     $('#tanggal_awal').datetimepicker({
        format: 'yyyy-mm-dd',
        language: 'id',
        todayBtn: 'linked',
        autoclose: true,
        minView : 2,
        maxView : 2,
        startView : 2,
        isInline : true
    });

    $('#tanggal_akhir').datetimepicker({
        format: 'yyyy-mm-dd',
        language: 'id',
        todayBtn: 'linked',
        autoclose: true,
        minView : 2,
        maxView : 2,
        startView : 2,
        isInline : true
    });

    //today = moment().format('YYYY-MM-DD');
    //prevDay = moment().subtract(9 ,'days').format('YYYY-MM-DD');
    //$('#tanggal_awal').val(prevDay);
    //$('#tanggal_akhir').val(today);
    filterTimeline();

    
  });
</script>
@endsection

@section('content')
<div class="modal" id="dialog-form" style="max-height: 5px">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeDialog()"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Detail Kegiatan</h4>
      </div>
      <div class="modal-body col-lg-12" style="max-height: calc(100vh - 210px);overflow-y: auto;">
        <p>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeDialog()">Keluar</button>
        <button id='btn-submit' style="display:none" type="button" class="btn btn-primary">Kirim</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<div class="row">  
            <div class="col-md-12">
                <div class="box">
                  <div class="box-body">
                    <input class="form-control" type="text" id="tanggal_awal" style="width:10%; display: inline" placeholder="Tanggal Awal" value="{!! $tanggal_awal !!}">
                    <input class="form-control" type="text" id="tanggal_akhir" style="width:10%; display: inline; margin-left: 2px;" placeholder="Tanggal Akhir" value="{!! $tanggal_akhir !!}">
                    {!! Form::select('pegawai', $list_pegawai, null, $options = array('id' => 'pegawai', 'class' => 'form-control', 'style'=>'width:20%; display: inline; margin-left: 10px; margin-left: 10px')) !!}

                    {!! Form::select('status', $list_status, $selected_status, $options = array('id' => 'status', 'class' => 'form-control', 'style'=>'width:20%; display: inline; margin-left: 10px; margin-left: 10px')) !!}
                    <a class="btn btn-primary" href="#" onclick="filterTimeline()" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">Tampilkan</a>


                    <div id="timeline" style="margin-top: 30px">

                    </div>
                  </div>
                </div>
            </div><!-- /.col -->
          </div> 
@endsection