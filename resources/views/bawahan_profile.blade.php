@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li><a href={{ $base_url }}>{{ $page_title }}</a></li>
            <li class="active">{{ $pegawai->nama }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
{{ $pegawai->nama }}
@endsection

@section('extra_css')
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datetimepicker/datetimepicker3.css') }}">
@endsection

@section('extra_js')
<script src="{{ asset('admin-lte/plugins/datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/datetimepicker/locales/bootstrap-datetimepicker.id.js') }}"></script>
<script src="{{ asset('js/kinerja_harian_bawahan.js') }}"></script>
<script type="text/javascript">

  function filterTimeline(){    
    tanggal_awal = $('#tanggal_awal').val();
    tanggal_akhir = $('#tanggal_akhir').val();
    var filter = { 
      'start': tanggal_awal,
      'end' : tanggal_akhir,
      'pegawai_id' : {{$pegawai->getPegawaiId()}}
    };
    showTimeline(filter, "#timeline");
  }

  function filterTabelTahunanBulanan(){ 
    tahun = $('#tahun').val();
    var pegawai_id = {{$pegawai->getPegawaiId()}};   
    showTabelTahunanBulanan(pegawai_id, tahun, "#activity");
  }

  $(document).ready(function () {    

     $('#tanggal_awal').datetimepicker({
        format: 'yyyy-mm-dd',
        language: 'id',
        todayBtn: 'linked',
        autoclose: true,
        minView : 2,
        maxView : 2,
        startView : 2,
        isInline : true
    });

    $('#tanggal_akhir').datetimepicker({
        format: 'yyyy-mm-dd',
        language: 'id',
        todayBtn: 'linked',
        autoclose: true,
        minView : 2,
        maxView : 2,
        startView : 2,
        isInline : true
    });

    today = moment().format('YYYY-MM-DD');
    prevDay = moment().subtract(9 ,'days').format('YYYY-MM-DD');
    $('#tanggal_awal').val(prevDay);
    $('#tanggal_akhir').val(today);
    filterTimeline();

    tahun = $('#tahun').val();
    filterTabelTahunanBulanan();   
    
  });
</script>
@endsection

@section('content')
<div class="modal" id="dialog-form" style="max-height: 5px">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeDialog()"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Detail Kegiatan</h4>
      </div>
      <div class="modal-body col-lg-12" style="max-height: calc(100vh - 210px);overflow-y: auto;">
        <p>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeDialog()">Keluar</button>
        <button id='btn-submit' style="display:none" type="button" class="btn btn-primary">Kirim</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<div class="row">
            <div class="col-md-12">
              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">   
                  <div class="col-md-3">    
                  @if( $pegawai->foto && file_exists( public_path().'/upload/'.$pegawai->foto) )
                  <img src="{{ asset('upload/'.$pegawai->foto) }}" class="profile-user-img img-responsive img-circle" alt="User Image">
                  @else
                  <img src="{{ asset('user.jpg') }}" class="profile-user-img img-responsive img-circle" alt="User Image">
                  @endif
                  <h3 class="profile-username text-center">{{ $pegawai->nama }}</h3>
                  <p class="text-muted text-center">{{ $pegawai->nip_baru }}</p>
                  </div>
                  
                  <div class="col-md-9">
                  <strong><i class="fa fa-bookmark margin-r-5"></i> Jabatan</strong>
                  <p class="text-muted">{{ $pegawai->keterangan_posisi }}
                   {{ $pegawai->keterangan_unitk }}</p>
                                   
                  <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>
                  <p class="text-muted">{{ $pegawai->alamat }}</p>  
                  
                  </div>               
                </div><!-- /.box-body -->
              </div><!-- /.box -->              
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#timeline_tab" data-toggle="tab" aria-expanded="true">Kegiatan Harian</a></li>
                  <li class=""><a href="#activity_tab" data-toggle="tab" aria-expanded="false">Kinerja Tahunan dan Bulanan</a></li>
                  
                </ul>

                <div class="tab-content">                  
                  <div class="tab-pane active" id="timeline_tab">
                    <input class="form-control" type="text" id="tanggal_awal" style="width:30%; display: inline" placeholder="Tanggal Awal">
                    <input class="form-control" type="text" id="tanggal_akhir" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px" placeholder="Tanggal Akhir">

                    <a class="btn btn-primary" href="#" onclick="filterTimeline()" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">Filter</a>


                    <div id="timeline" style="margin-top: 30px">

                    </div>
                  </div><!-- /.tab-pane --> 

                  <div class="tab-pane"  id="activity_tab">
                   {!! Form::select('tahun', $list_tahun, null, $options = array('id' => 'tahun', 'class' => 'form-control', 'style'=>'width:30%; display: inline')) !!}

                   <a class="btn btn-primary" href="#" onclick="filterTabelTahunanBulanan()" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">Filter</a>
                   
                   <div id="activity" style="margin-top: 30px">

                   </div>
                  </div>          
                </div><!-- /.tab-content -->


              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div>
@endsection