@extends('form')

@section('content')

<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                   {!! $edit->header !!}
                   		{!! $edit->message !!}

                   		@if(!$edit->message)
                   			
                   			<div class="form-group clearfix">
                   			<label class="col-sm-2 control-label required">
                   				Tahun
                   			</label>                   			
                   			<div class="col-sm-10">
                   				<div class="help-block">
                   				@if ($edit->status == 'create')
		                        	{!! $tahun !!}
		                        @else
		                        	{!! $edit->field('tahun') !!}
		                        @endif
		                        </div>
		                    </div>
		                    </div>		                    

		                    @if(isset($nama_pegawai))
		                    <div class="form-group clearfix {!! $edit->field('nama')->has_error !!}">
                   			<label class="col-sm-2 control-label{!! $edit->field('nama')->req !!}">
                   				Nama Pegawai
                   			</label>                   			
                   			<div class="col-sm-10">
		                        {!! $nama_pegawai !!}
		                    </div>
		                    </div>
		                    @endif

							<div class="form-group clearfix {!! $edit->field('periode_awal')->has_error !!}">
                   			<label class="col-sm-2 control-label{!! $edit->field('periode_awal')->req !!}">
                   				Periode
                   			</label>                   			
                   			<div class="col-sm-3">
		                        {!! $edit->field('periode_awal') !!}
		                        <label>{!! $edit->field('periode_awal')->message !!}</label>
		                    </div>
							<div class="col-sm-1">
		                        <div style="padding-top:0px;text-align:center;"> sampai </div>
		                    </div>
							<div class="col-sm-3">
		                        {!! $edit->field('periode_akhir') !!}
		                        <label>{!! $edit->field('periode_akhir')->message !!}</label>
		                    </div>
		                    </div>

                   			<div class="form-group clearfix {!! $edit->field('nama')->has_error !!}">
                   			<label class="col-sm-2 control-label{!! $edit->field('nama')->req !!}">
                   				{!! $edit->field('nama')->label !!}
                   			</label>                   			
                   			<div class="col-sm-10">
		                        {!! $edit->field('nama') !!}
		                        <label>{!! $edit->field('nama')->message !!}</label>
		                    </div>
		                    </div>

		                    <div class="form-group clearfix {!! $edit->field('deskripsi')->has_error !!}">
	                        <label class="col-sm-2 control-label{!! $edit->field('deskripsi')->req !!}">
	                          {!! $edit->field('deskripsi')->label !!}
	                        </label>                        
	                        <div class="col-sm-10">              	    @if($edit->status == 'show')
	                            {!! $edit->field('deskripsi')->value !!}
	                            @else
	                            {!! $edit->field('deskripsi') !!}
	                            @endif
	                            <label>{!! $edit->field('deskripsi')->message !!}</label>
	                        </div>
	                        </div>

		                    <div class="form-group clearfix {!! $edit->field('target_kuantitas')->has_error !!}">
		                    <label class="col-sm-2 control-label{!! $edit->field('target_kuantitas')->req !!}">
                   				{!! $edit->field('target_kuantitas')->label !!}
                   			</label>
                   			<div class="col-sm-2">
		                        {!! $edit->field('target_kuantitas') !!}
                            <label>{!! $edit->field('target_kuantitas')->message !!}</label>
		                    </div>                   
		                    <div class="col-sm-5">
		                        {!! $edit->field('satuan_target_kuantitas') !!}
		                    </div>                        
		                    </div>

		                    <div class="form-group clearfix {!! $edit->field('target_waktu')->has_error !!}">
		                    <label class="col-sm-2 control-label{!! $edit->field('target_waktu')->req !!}">
                   				{!! $edit->field('target_waktu')->label !!}
                   			</label>
                   			<div class="col-sm-2">
		                        {!! $edit->field('target_waktu') !!}
                            <label>{!! $edit->field('target_waktu')->message !!}</label>
		                    </div>                   
		                    <div class="col-sm-5" style="padding-top:5px">
								bulan
		                    </div>                        
		                    </div>

		                    <div class="form-group clearfix {!! $edit->field('target_kualitas')->has_error !!}">
		                    <label class="col-sm-2 control-label{!! $edit->field('target_kualitas')->req !!}">
                   				{!! $edit->field('target_kualitas')->label !!}
                   			</label>
		                    <div class="col-sm-5">
		                        {!! $edit->field('target_kualitas') !!}
		                        <label>{!! $edit->field('target_kualitas')->message !!}</label>
		                    </div>
		                    </div>

		                    @if ($edit->status == 'show')
		                    <div class="form-group clearfix">
		                    <label class="col-sm-2 control-label{!! $edit->field('capaian_kuantitas')->req !!}">
                   				{!! $edit->field('capaian_kuantitas')->label !!}
                   			</label>
		                    <div class="col-sm-1">
		                        {!! $edit->field('capaian_kuantitas') !!}
		                    </div>		                    
		                    <div class="col-sm-5">
		                        {!! $edit->field('satuan_target_kuantitas') !!}
		                    </div>
		                    </div>

		                    <div class="form-group clearfix">
		                    <label class="col-sm-2 control-label{!! $edit->field('capaian_waktu')->req !!}">
                   				{!! $edit->field('capaian_waktu')->label !!}
                   			</label>
		                    <div class="col-sm-1">
		                        {!! $edit->field('capaian_waktu') !!}
		                    </div>		                    
		                    <div class="col-sm-5">
		                        {!! $edit->field('satuan_target_waktu') !!}
		                    </div>
		                    </div>

		                    <div class="form-group clearfix">
		                    <label class="col-sm-2 control-label{!! $edit->field('capaian_kualitas')->req !!}">
                   				{!! $edit->field('capaian_kualitas')->label !!}
                   			</label>
		                    <div class="col-sm-5">
		                        {!! $edit->field('capaian_kualitas') !!}
		                    </div>
		                    </div>
		                    @endif
                   		@endif
                   		<br/>                   		
                   {!! $edit->footer !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
@endsection