@extends('base')

@section('breadcrumb')
            <li><a href="/">Home</a></li>
            <li class="active">{{ $page_title }}</li>
@endsection

@section('page_title')
{{ $page_title }}
@endsection

@section('second_page_title')
Daftar
@endsection

@section('extra_css')

@endsection

@section('extra_js')
<script type="text/javascript">
function getTable(){
  startLoading();
  tahun = $('#tahun').val();
  bulan = $('#bulan').val();
  $.ajax({
    url: '/kinerja_bulanan/gajibulanan',
    data: { 
      'tahun': tahun,
      'bulan': bulan
    },
    success: function(data) {
      stopLoading();
      $('#tabel').html(data);
    },
    error: function(xhr, status, error) {
      stopLoading();
      console(xhr.responseText);
      alert("Terjadi Kesalahan pada Sistem");
    }
  });
}

$(document).ready(function () {
  getTable();    
});
  
</script>
@endsection

@section('content')
<div class="modal" id="dialog-form" style="max-height: 5px">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeDialog()"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Detail Kegiatan</h4>
      </div>
      <div class="modal-body col-lg-12" style="max-height: calc(100vh - 210px);overflow-y: auto;">
        <p>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeDialog()">Keluar</button>
        <button id='btn-submit' style="display:none" type="button" class="btn btn-primary">Kirim</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<div class="row">  
            <div class="col-md-12">
                <div class="box">
                  <div class="box-body">                    
                    {!! Form::select('bulan', $list_bulan, $selected_bulan, $options = array('id' => 'bulan', 'class' => 'form-control', 'style'=>'width:20%; display: inline; margin-left: 10px; margin-left: 10px')) !!}

                    {!! Form::select('tahun', $list_tahun, null, $options = array('id' => 'tahun', 'class' => 'form-control', 'style'=>'width:20%; display: inline; margin-left: 10px; margin-left: 10px')) !!}
                    <a class="btn btn-primary" href="#" onclick="getTable()" style="width:30%; display: inline; margin-left: 10px; margin-left: 10px">Filter</a>


                    <div id="tabel" style="margin-top: 30px">

                    </div>
                  </div>
                </div>
            </div><!-- /.col -->
          </div> 
@endsection