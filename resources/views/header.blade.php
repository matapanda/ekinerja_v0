@section('extra_js_header')
<script type="text/javascript">
  var jumlah_notif_pegawai = 0;
  var jumlah_notif_atasan = 0; 

  function tampilkanNotifikasi(){
    var total_notif = parseInt(jumlah_notif_pegawai) + parseInt(jumlah_notif_atasan); 
    $('#jumlah_notifikasi').html(total_notif);
    $('#jumlah_notifikasi_panjang').html(total_notif);
    $('.jumlah_notifikasi_pegawai').html(jumlah_notif_pegawai);
    $('.jumlah_notifikasi_atasan').html(jumlah_notif_atasan);
  }

  function getNotifikasiSebagaiPegawai(){
    $.ajax({
      url: '/notifikasi/pegawai',
      data: { 
          
      },
      success: function(data) {
        jumlah_notif_pegawai = data;
        tampilkanNotifikasi();
      }
    });
  } 

  function getNotifikasiSebagaiAtasan(){
    $.ajax({
      url: '/notifikasi/atasan',
      data: { 
          
      },
      success: function(data) {
        jumlah_notif_atasan = data; 
        tampilkanNotifikasi();
      }
    });
  } 

  $(document).ready(function () {
    getNotifikasiSebagaiAtasan();
    getNotifikasiSebagaiPegawai();
  });  
</script>
@endsection
<header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>e</b>Kinerja</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>e</b>Kinerja</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div style=" float: left; background-color: transparent;background-image: none;
    padding: 15px 15px; color: #fff">
          <!--
          $pegawai_global->getNamaUnitSubUnitK() 
          -->
          </div>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-danger" id="jumlah_notifikasi">0</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Anda memiliki <i id="jumlah_notifikasi_panjang">0</i> notifikasi</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;"><ul class="menu" style="overflow: hidden; width: 100%; height: 100px;">
                      <li>
                        <a href="{{ url('kinerja_harian_bawahan?status=1&semuatanggal=1') }}">
                          <i class="fa fa-users text-aqua"></i> <i id="jumlah_notifikasi_atasan" class="jumlah_notifikasi_atasan">0</i> kegiatan bawahan membutuhkan konfirmasi anda.
                        </a>
                      </li>
                      
                      <li>
                        <a href="{{url('kinerja_harian?nama=&tanggal%5Bfrom%5D=&tanggal%5Bto%5D=&status=3&search=1')}}">
                          <i class="fa fa-users text-red"></i> <i id="jumlah_notifikasi_pegawai" class="jumlah_notifikasi_pegawai">0</i> kegiatan anda ditolak oleh atasan.
                        </a>
                      </li>
                      
                      
                    </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 195.122px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                  </li>
                  
                </ul>
              </li>
                         
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  @if( $pegawai_global->foto && file_exists( public_path().'/upload/'.$pegawai_global->foto) )
              <img src="{{ asset('upload/'.$pegawai_global->foto) }}" class="user-image" alt="User Image">
              @else           
              <img src="{{ asset('user.jpg') }}" class="user-image" alt="User Image">
              @endif
                  <span class="hidden-xs">{{ Auth::user()->nama }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                     @if( $pegawai_global->foto && file_exists( public_path().'/upload/'.$pegawai_global->foto) )
                    <img src="{{ asset('upload/'.$pegawai_global->foto) }}" class="img-circle" alt="User Image">
                    @else
                    <img src="{{ asset('user.jpg') }}" class="img-circle" alt="User Image">
                    @endif
                    <p>
                      {{ $pegawai_global->nama }}
                    </p>
                  </li>
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="/pilih_atasan" class="btn btn-default btn-flat">Ubah Atasan</a>
                    </div>
                    <div class="pull-right">
                      <a href="/auth/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              
            </ul>
          </div>
        </nav>
      </header>