<?php

return array(
    'save'    => 'Simpan',
    'undo'    => 'Batal',
    'show'    => 'Tampil',
    'modify'  => 'Ubah',
    'delete'  => 'Hapus',
    'add'     => 'Tambah',
    'reset'   => 'Reset',
    'search'  => 'Cari',
    'back'    => 'Kembali',

    //dataedit
    'inserted'   => 'Data baru sudah ditambahkan.',
    'updated'    => 'Data telah diubah.',
    'deleted'    => 'Data telah dihapus.',
    'err'        => 'Terdapat kesalahan pembacaan data.',
    'err_unknown'=> 'Error, tidak ada data yang ditemukan',
    'err_dup_pk' => 'Error, primary key terduplikasi',
    'no_records' => 'Tidak ada data yang ditampilkan.',
    'conf_delete'=> 'Apakah anda yakin akan menghapus data ini?',

);
